/* Driver template for the LEMON parser generator.
** The author disclaims copyright to this source code.
*/
/* First off, code is included that follows the "include" declaration
** in the input grammar file. */
#include <stdio.h>
#line 22 "parse.y"
	/*	-*- c -*-	*/
# include	<ctype.h>
# include	<assert.h>	
# include	"grammar.h"
# include	"parse.h"

	static inline void
	perr (private_t *priv, const char *str)
	{
		if (priv -> parse_error) {
			buffer_appends (priv -> parse_error, str);
			buffer_appendch (priv -> parse_error, '\n');
		}
	}

	static inline buffer_t *
	tok2buf (token_t *t)
	{
		buffer_t	*b = buffer_alloc (256);
		
		buffer_sets (b, t -> token);
		token_free (t);
		return b;
	}
	
	static inline buffer_t *
	operation (buffer_t *a, const char *op, buffer_t *b)
	{
		buffer_t	*r = buffer_alloc (a -> length + b -> length + 16);
		
		buffer_format (r, "(%s) %s (%s)", buffer_string (a), op, buffer_string (b));
		buffer_free (a);
		buffer_free (b);
		return r;
	}
	
	static inline buffer_t *
	conversion (const char *funcname, buffer_t *a, xconv_t *xconv, const xchar_t *(*func) (xconv_t *, const xchar_t *, int, int *))
	{
		const char	*str = buffer_string (a);
		bool_t		purestring = false;
		buffer_t	*r = buffer_alloc (a -> length + 64);
		const xchar_t	*rplc;
		int		rlen;

		if (*str == '"') {
			do {
				if ((*str == '\\') && *(str + 1))
					++str;
				++str;
			}	while (*str && (*str != '"'));
			if ((*str == '"') && (! *(str + 1)))
				purestring = true;
		}
		if (purestring && (rplc = (*func) (xconv, (const xchar_t *) a -> buffer, a -> length, & rlen)))
			buffer_append (r, rplc, rlen);
		else
			buffer_format (r, "%s (%s)", funcname, buffer_string (a));
		buffer_free (a);
		return r;
	}
#line 70 "parse.c"
/* Next is all token values, in a form suitable for use by makeheaders.
** This section will be null unless lemon is run with the -m switch.
*/
/* 
** These constants (all generated automatically by the parser generator)
** specify the various kinds of tokens (terminals) that the parser
** understands. 
**
** Each symbol here is a terminal symbol in the grammar.
*/
/* Make sure the INTERFACE macro is defined.
*/
#ifndef INTERFACE
# define INTERFACE 1
#endif
/* The next thing included is series of defines which control
** various aspects of the generated parser.
**    YYCODETYPE         is the data type used for storing terminal
**                       and nonterminal numbers.  "unsigned char" is
**                       used if there are fewer than 250 terminals
**                       and nonterminals.  "int" is used otherwise.
**    YYNOCODE           is a number of type YYCODETYPE which corresponds
**                       to no legal terminal or nonterminal number.  This
**                       number is used to fill in empty slots of the hash 
**                       table.
**    YYFALLBACK         If defined, this indicates that one or more tokens
**                       have fall-back values which should be used if the
**                       original value of the token will not parse.
**    YYACTIONTYPE       is the data type used for storing terminal
**                       and nonterminal numbers.  "unsigned char" is
**                       used if there are fewer than 250 rules and
**                       states combined.  "int" is used otherwise.
**    ParseTOKENTYPE     is the data type used for minor tokens given 
**                       directly to the parser from the tokenizer.
**    YYMINORTYPE        is the data type used for all minor tokens.
**                       This is typically a union of many types, one of
**                       which is ParseTOKENTYPE.  The entry in the union
**                       for base tokens is called "yy0".
**    YYSTACKDEPTH       is the maximum depth of the parser's stack.  If
**                       zero the stack is dynamically sized using realloc()
**    ParseARG_SDECL     A static variable declaration for the %extra_argument
**    ParseARG_PDECL     A parameter declaration for the %extra_argument
**    ParseARG_STORE     Code to store %extra_argument into yypParser
**    ParseARG_FETCH     Code to extract %extra_argument from yypParser
**    YYNSTATE           the combined number of states.
**    YYNRULE            the number of rules in the grammar
**    YYERRORSYMBOL      is the code number of the error symbol.  If not
**                       defined, then do no error processing.
*/
#define YYCODETYPE unsigned char
#define YYNOCODE 43
#define YYACTIONTYPE unsigned char
#define ParseTOKENTYPE token_t *
typedef union {
  int yyinit;
  ParseTOKENTYPE yy0;
  buffer_t * yy28;
} YYMINORTYPE;
#ifndef YYSTACKDEPTH
#define YYSTACKDEPTH 0
#endif
#define ParseARG_SDECL private_t *priv;
#define ParseARG_PDECL ,private_t *priv
#define ParseARG_FETCH private_t *priv = yypParser->priv
#define ParseARG_STORE yypParser->priv = priv
#define YYNSTATE 121
#define YYNRULE 43
#define YY_NO_ACTION      (YYNSTATE+YYNRULE+2)
#define YY_ACCEPT_ACTION  (YYNSTATE+YYNRULE+1)
#define YY_ERROR_ACTION   (YYNSTATE+YYNRULE)

/* The yyzerominor constant is used to initialize instances of
** YYMINORTYPE objects to zero. */
static const YYMINORTYPE yyzerominor = { 0 };


/* Next are the tables used to determine what action to take based on the
** current state and lookahead token.  These tables are used to implement
** functions that take a state number and lookahead value and return an
** action integer.  
**
** Suppose the action integer is N.  Then the action is determined as
** follows
**
**   0 <= N < YYNSTATE                  Shift N.  That is, push the lookahead
**                                      token onto the stack and goto state N.
**
**   YYNSTATE <= N < YYNSTATE+YYNRULE   Reduce by rule N-YYNSTATE.
**
**   N == YYNSTATE+YYNRULE              A syntax error has occurred.
**
**   N == YYNSTATE+YYNRULE+1            The parser accepts its input.
**
**   N == YYNSTATE+YYNRULE+2            No such action.  Denotes unused
**                                      slots in the yy_action[] table.
**
** The action table is constructed as a single large table named yy_action[].
** Given state S and lookahead X, the action is computed as
**
**      yy_action[ yy_shift_ofst[S] + X ]
**
** If the index value yy_shift_ofst[S]+X is out of range or if the value
** yy_lookahead[yy_shift_ofst[S]+X] is not equal to X or if yy_shift_ofst[S]
** is equal to YY_SHIFT_USE_DFLT, it means that the action is not in the table
** and that yy_default[S] should be used instead.  
**
** The formula above is for computing the action when the lookahead is
** a terminal symbol.  If the lookahead is a non-terminal (as occurs after
** a reduce action) then the yy_reduce_ofst[] array is used in place of
** the yy_shift_ofst[] array and YY_REDUCE_USE_DFLT is used in place of
** YY_SHIFT_USE_DFLT.
**
** The following are the tables generated in this section:
**
**  yy_action[]        A single table containing all actions.
**  yy_lookahead[]     A table containing the lookahead for each entry in
**                     yy_action.  Used to detect hash collisions.
**  yy_shift_ofst[]    For each state, the offset into yy_action for
**                     shifting terminals.
**  yy_reduce_ofst[]   For each state, the offset into yy_action for
**                     shifting non-terminals after a reduce.
**  yy_default[]       Default action for each state.
*/
static const YYACTIONTYPE yy_action[] = {
 /*     0 */   138,  138,  138,  138,  138,  138,  138,   66,  138,  138,
 /*    10 */   138,  138,  138,  138,  138,  138,   14,   15,   76,  138,
 /*    20 */   138,  162,  162,  162,  162,  162,  162,  162,   71,  162,
 /*    30 */   162,  162,  162,  162,  162,  162,  162,  162,  162,   94,
 /*    40 */   162,  162,  156,  156,  156,  156,  156,  156,  156,   63,
 /*    50 */   156,  156,  156,  156,  156,  156,  156,  156,  156,  156,
 /*    60 */    74,  156,  156,  163,  163,  163,  163,  163,  163,  163,
 /*    70 */    67,  163,  163,  163,  163,  163,  163,  163,  163,  163,
 /*    80 */   163,   42,  163,  163,  145,  145,  145,  145,  145,  145,
 /*    90 */   145,   35,  145,  145,  145,  145,  145,  145,  145,  145,
 /*   100 */   145,  145,   44,  145,  145,  146,  146,  146,  146,  146,
 /*   110 */   146,  146,    6,  146,  146,  146,  146,  146,  146,  146,
 /*   120 */   146,  146,  146,   77,  146,  146,  154,  154,  154,  154,
 /*   130 */   154,  154,  154,   28,  154,  154,  154,  154,  154,  154,
 /*   140 */   154,  154,  154,  154,   46,  154,  154,  137,  137,  137,
 /*   150 */   137,  137,  137,  137,   25,  137,  137,  137,  137,  137,
 /*   160 */   137,  137,  137,  137,  137,   88,  137,  137,  134,  134,
 /*   170 */   134,  134,  134,  134,  134,  116,  134,  134,  134,  134,
 /*   180 */   134,  134,  134,  134,   14,   15,  117,  134,  134,  150,
 /*   190 */   150,  150,  150,  150,  150,  150,   86,  150,  150,  150,
 /*   200 */   150,  150,  150,  150,  150,  150,  150,   50,  150,  150,
 /*   210 */   155,  155,  155,  155,  155,  155,  155,  114,  155,  155,
 /*   220 */   155,  155,  155,  155,  155,  155,  155,  155,   79,  155,
 /*   230 */   155,  147,  147,  147,  147,  147,  147,  147,   30,  147,
 /*   240 */   147,  147,  147,  147,  147,  147,  147,  147,  147,   49,
 /*   250 */   147,  147,  149,  149,  149,  149,  149,  149,  149,   20,
 /*   260 */   149,  149,  149,  149,  149,  149,  149,  149,  149,  149,
 /*   270 */   166,  149,  149,  148,  148,  148,  148,  148,  148,  148,
 /*   280 */    81,  148,  148,  148,  148,  148,  148,  148,  148,  148,
 /*   290 */   148,   70,  148,  148,  153,  153,  153,  153,  153,  153,
 /*   300 */   153,    5,  153,  153,  153,  153,  153,  153,  153,  153,
 /*   310 */   153,  153,   64,  153,  153,  136,  136,  136,  136,  136,
 /*   320 */   136,  136,   78,  136,  136,  136,  136,  136,  136,  136,
 /*   330 */   136,  136,  136,   84,  136,  136,  151,  151,  151,  151,
 /*   340 */   151,  151,  151,   68,  151,  151,  151,  151,  151,  151,
 /*   350 */   151,  151,  151,  151,   83,  151,  151,  152,  152,  152,
 /*   360 */   152,  152,  152,  152,   40,  152,  152,  152,  152,  152,
 /*   370 */   152,  152,  152,  152,  152,   69,  152,  152,  135,  135,
 /*   380 */   135,  135,  135,  135,  135,   62,  135,  135,  135,  135,
 /*   390 */   135,  135,  135,  135,   14,   15,   65,  135,  135,  157,
 /*   400 */   157,  157,  157,  157,  157,  157,    8,  157,  157,  157,
 /*   410 */   157,  157,  157,  157,  157,  157,  157,   82,  157,  157,
 /*   420 */   158,  158,  158,  158,  158,  158,  158,   32,  158,  158,
 /*   430 */   158,  158,  158,  158,  158,  158,  158,  158,    4,  158,
 /*   440 */   158,   37,   13,   14,   15,   85,   55,   34,    7,   31,
 /*   450 */    16,  109,   29,    2,   72,  101,  115,  119,  106,  108,
 /*   460 */   110,  111,  105,  113,  112,   56,   51,   59,   33,  118,
 /*   470 */   107,   48,   16,    3,    1,   17,   72,  102,  115,  119,
 /*   480 */   106,  108,  110,  111,  105,  113,  112,   56,   51,   41,
 /*   490 */   166,   89,  107,   48,   90,   10,  158,  158,  158,  158,
 /*   500 */    99,  158,  158,  158,  158,  158,  158,  158,  158,  158,
 /*   510 */   158,   80,  158,   97,   26,   24,   12,   73,  120,    9,
 /*   520 */    23,   21,   19,   18,   37,   13,   14,   15,  104,   53,
 /*   530 */    97,   26,   24,   12,   75,  120,    9,   23,   21,   19,
 /*   540 */    18,   37,   13,   14,   15,  133,  133,  133,   37,   13,
 /*   550 */    14,   15,  166,  140,  140,  165,   96,   61,   91,   37,
 /*   560 */    13,   14,   15,   27,  133,  143,  143,  143,   37,   13,
 /*   570 */    14,   15,  166,  139,  139,   37,   13,   14,   15,   37,
 /*   580 */    13,   14,   15,   11,  143,  132,  132,  132,   37,   13,
 /*   590 */    14,   15,  166,   53,  166,   37,   13,   14,   15,   37,
 /*   600 */    13,   14,   15,  166,  132,  131,  131,  131,   37,   13,
 /*   610 */    14,   15,  166,   45,  166,   87,   10,   92,   61,   37,
 /*   620 */    13,   14,   15,  166,  131,  130,  130,  130,   37,   13,
 /*   630 */    14,   15,  166,   54,  166,   39,   10,   57,   10,   37,
 /*   640 */    13,   14,   15,  166,  130,  129,  129,  129,   37,   13,
 /*   650 */    14,   15,  166,   43,  166,  166,   95,   61,  166,   37,
 /*   660 */    13,   14,   15,  166,  129,  128,  128,  128,   37,   13,
 /*   670 */    14,   15,  166,   58,  166,   98,   60,   93,   61,   37,
 /*   680 */    13,   14,   15,  166,  128,  127,  127,  127,   37,   13,
 /*   690 */    14,   15,  166,   52,  166,  166,  166,   72,  103,   37,
 /*   700 */    13,   14,   15,  166,  127,  126,  126,  126,   37,   13,
 /*   710 */    14,   15,   72,  100,   36,  166,  166,  166,  166,   37,
 /*   720 */    13,   14,   15,  166,  126,  144,  144,  144,  166,   37,
 /*   730 */    13,   14,   15,  166,  166,   22,  121,    3,    1,   37,
 /*   740 */    13,   14,   15,  166,  144,   37,   13,   14,   15,  166,
 /*   750 */    38,  159,  159,  159,   37,   13,   14,   15,  166,   47,
 /*   760 */   125,  125,  125,  161,  161,  161,  122,  122,  122,  166,
 /*   770 */   159,  123,  123,    1,  141,  141,  141,  166,  166,  125,
 /*   780 */   166,  166,  161,  166,  166,  122,  124,  124,  124,  166,
 /*   790 */   123,  166,  166,  141,  160,  160,  160,  142,  142,  142,
 /*   800 */   166,  166,  166,  166,  166,  124,  166,  166,  166,  166,
 /*   810 */   166,  166,  166,  160,  166,  166,  142,
};
static const YYCODETYPE yy_lookahead[] = {
 /*     0 */     0,    1,    2,    3,    4,    5,    6,   40,    8,    9,
 /*    10 */    10,   11,   12,   13,   14,   15,   16,   17,   40,   19,
 /*    20 */    20,    0,    1,    2,    3,    4,    5,    6,   40,    8,
 /*    30 */     9,   10,   11,   12,   13,   14,   15,   16,   17,   33,
 /*    40 */    19,   20,    0,    1,    2,    3,    4,    5,    6,   40,
 /*    50 */     8,    9,   10,   11,   12,   13,   14,   15,   16,   17,
 /*    60 */    40,   19,   20,    0,    1,    2,    3,    4,    5,    6,
 /*    70 */    40,    8,    9,   10,   11,   12,   13,   14,   15,   16,
 /*    80 */    17,   40,   19,   20,    0,    1,    2,    3,    4,    5,
 /*    90 */     6,   18,    8,    9,   10,   11,   12,   13,   14,   15,
 /*   100 */    16,   17,   40,   19,   20,    0,    1,    2,    3,    4,
 /*   110 */     5,    6,   18,    8,    9,   10,   11,   12,   13,   14,
 /*   120 */    15,   16,   17,   40,   19,   20,    0,    1,    2,    3,
 /*   130 */     4,    5,    6,   18,    8,    9,   10,   11,   12,   13,
 /*   140 */    14,   15,   16,   17,   40,   19,   20,    0,    1,    2,
 /*   150 */     3,    4,    5,    6,   18,    8,    9,   10,   11,   12,
 /*   160 */    13,   14,   15,   16,   17,   19,   19,   20,    0,    1,
 /*   170 */     2,    3,    4,    5,    6,   18,    8,    9,   10,   11,
 /*   180 */    12,   13,   14,   15,   16,   17,   32,   19,   20,    0,
 /*   190 */     1,    2,    3,    4,    5,    6,   40,    8,    9,   10,
 /*   200 */    11,   12,   13,   14,   15,   16,   17,   40,   19,   20,
 /*   210 */     0,    1,    2,    3,    4,    5,    6,   18,    8,    9,
 /*   220 */    10,   11,   12,   13,   14,   15,   16,   17,   40,   19,
 /*   230 */    20,    0,    1,    2,    3,    4,    5,    6,   18,    8,
 /*   240 */     9,   10,   11,   12,   13,   14,   15,   16,   17,   19,
 /*   250 */    19,   20,    0,    1,    2,    3,    4,    5,    6,   18,
 /*   260 */     8,    9,   10,   11,   12,   13,   14,   15,   16,   17,
 /*   270 */    42,   19,   20,    0,    1,    2,    3,    4,    5,    6,
 /*   280 */    40,    8,    9,   10,   11,   12,   13,   14,   15,   16,
 /*   290 */    17,   40,   19,   20,    0,    1,    2,    3,    4,    5,
 /*   300 */     6,   18,    8,    9,   10,   11,   12,   13,   14,   15,
 /*   310 */    16,   17,   40,   19,   20,    0,    1,    2,    3,    4,
 /*   320 */     5,    6,   40,    8,    9,   10,   11,   12,   13,   14,
 /*   330 */    15,   16,   17,   40,   19,   20,    0,    1,    2,    3,
 /*   340 */     4,    5,    6,   40,    8,    9,   10,   11,   12,   13,
 /*   350 */    14,   15,   16,   17,   40,   19,   20,    0,    1,    2,
 /*   360 */     3,    4,    5,    6,   40,    8,    9,   10,   11,   12,
 /*   370 */    13,   14,   15,   16,   17,   40,   19,   20,    0,    1,
 /*   380 */     2,    3,    4,    5,    6,   40,    8,    9,   10,   11,
 /*   390 */    12,   13,   14,   15,   16,   17,   40,   19,   20,    0,
 /*   400 */     1,    2,    3,    4,    5,    6,   18,    8,    9,   10,
 /*   410 */    11,   12,   13,   14,   15,   16,   17,   40,   19,   20,
 /*   420 */     0,    1,    2,    3,    4,    5,    6,   18,    8,    9,
 /*   430 */    10,   11,   12,   13,   14,   15,   16,   17,    3,   19,
 /*   440 */    20,   14,   15,   16,   17,   40,   19,   20,   18,    6,
 /*   450 */    15,    8,    9,   18,   40,   41,   21,   22,   23,   24,
 /*   460 */    25,   26,   27,   28,   29,   30,   31,   32,   18,   34,
 /*   470 */    35,   36,   15,    1,    2,   18,   40,   41,   21,   22,
 /*   480 */    23,   24,   25,   26,   27,   28,   29,   30,   31,   32,
 /*   490 */    42,   19,   35,   36,   19,   20,    3,    4,    5,    6,
 /*   500 */     7,    8,    9,   10,   11,   12,   13,   14,   15,   16,
 /*   510 */    17,   40,   19,    3,    4,    5,    6,   40,    8,    9,
 /*   520 */    10,   11,   12,   13,   14,   15,   16,   17,    3,   19,
 /*   530 */     3,    4,    5,    6,   40,    8,    9,   10,   11,   12,
 /*   540 */    13,   14,   15,   16,   17,    0,    1,    2,   14,   15,
 /*   550 */    16,   17,   42,   19,   20,   38,   39,   40,   33,   14,
 /*   560 */    15,   16,   17,    2,   19,    0,    1,    2,   14,   15,
 /*   570 */    16,   17,   42,   19,   20,   14,   15,   16,   17,   14,
 /*   580 */    15,   16,   17,    2,   19,    0,    1,    2,   14,   15,
 /*   590 */    16,   17,   42,   19,   42,   14,   15,   16,   17,   14,
 /*   600 */    15,   16,   17,   42,   19,    0,    1,    2,   14,   15,
 /*   610 */    16,   17,   42,   19,   42,   19,   20,   39,   40,   14,
 /*   620 */    15,   16,   17,   42,   19,    0,    1,    2,   14,   15,
 /*   630 */    16,   17,   42,   19,   42,   19,   20,   19,   20,   14,
 /*   640 */    15,   16,   17,   42,   19,    0,    1,    2,   14,   15,
 /*   650 */    16,   17,   42,   19,   42,   42,   39,   40,   42,   14,
 /*   660 */    15,   16,   17,   42,   19,    0,    1,    2,   14,   15,
 /*   670 */    16,   17,   42,   19,   42,   39,   40,   39,   40,   14,
 /*   680 */    15,   16,   17,   42,   19,    0,    1,    2,   14,   15,
 /*   690 */    16,   17,   42,   19,   42,   42,   42,   40,   41,   14,
 /*   700 */    15,   16,   17,   42,   19,    0,    1,    2,   14,   15,
 /*   710 */    16,   17,   40,   41,   20,   42,   42,   42,   42,   14,
 /*   720 */    15,   16,   17,   42,   19,    0,    1,    2,   42,   14,
 /*   730 */    15,   16,   17,   42,   42,   20,    0,    1,    2,   14,
 /*   740 */    15,   16,   17,   42,   19,   14,   15,   16,   17,   42,
 /*   750 */    19,    0,    1,    2,   14,   15,   16,   17,   42,   19,
 /*   760 */     0,    1,    2,    0,    1,    2,    0,    1,    2,   42,
 /*   770 */    19,    0,    1,    2,    0,    1,    2,   42,   42,   19,
 /*   780 */    42,   42,   19,   42,   42,   19,    0,    1,    2,   42,
 /*   790 */    19,   42,   42,   19,    0,    1,    2,    0,    1,    2,
 /*   800 */    42,   42,   42,   42,   42,   19,   42,   42,   42,   42,
 /*   810 */    42,   42,   42,   19,   42,   42,   19,
};
#define YY_SHIFT_USE_DFLT (-1)
#define YY_SHIFT_MAX 120
static const short yy_shift_ofst[] = {
 /*     0 */   435,  435,  435,  435,  435,  457,  457,  457,  457,  457,
 /*    10 */   457,  457,  457,  457,  457,  457,  457,  457,  457,  457,
 /*    20 */   457,  457,  457,  457,  457,  457,  457,  457,  457,  457,
 /*    30 */   457,  457,  457,  457,  457,  457,  457,  457,  231,  210,
 /*    40 */   168,  420,  378,  336,  315,  273,  147,  105,   63,   21,
 /*    50 */     0,  399,  294,   84,  252,  357,   42,  126,  189,  493,
 /*    60 */   510,  527,  725,  705,  685,  665,  645,  625,  605,  585,
 /*    70 */   545,  565,  554,  534,  427,  674,  654,  634,  614,  574,
 /*    80 */   715,  694,  581,  594,  561,  731,  740,  797,  794,  786,
 /*    90 */   774,  751,  771,  766,  763,  760,  736,  443,  472,  525,
 /*   100 */   618,  616,  596,  475,    6,   73,  115,  199,  220,  430,
 /*   110 */   409,  450,  388,  283,  230,  241,  154,  146,  157,  136,
 /*   120 */    94,
};
#define YY_REDUCE_USE_DFLT (-34)
#define YY_REDUCE_MAX 37
static const short yy_reduce_ofst[] = {
 /*     0 */   517,  638,  636,  578,  617,  672,  657,  436,  414,  377,
 /*    10 */   477,  -12,    9,   41,   62,  104,  167,  188,  251,  335,
 /*    20 */   471,  303,  156,   30,  -33,  405,  356,  345,  314,  293,
 /*    30 */   282,  272,  -22,   20,   83,  240,  494,  324,
};
static const YYACTIONTYPE yy_default[] = {
 /*     0 */   164,  164,  164,  164,  164,  164,  164,  164,  164,  164,
 /*    10 */   164,  164,  164,  164,  164,  164,  164,  164,  164,  164,
 /*    20 */   164,  164,  164,  164,  164,  164,  164,  164,  164,  164,
 /*    30 */   164,  164,  164,  164,  164,  164,  164,  164,  164,  164,
 /*    40 */   164,  164,  164,  164,  164,  164,  164,  164,  164,  164,
 /*    50 */   164,  164,  164,  164,  164,  164,  164,  164,  164,  164,
 /*    60 */   164,  164,  164,  164,  164,  164,  164,  164,  164,  164,
 /*    70 */   164,  164,  164,  164,  164,  164,  164,  164,  164,  164,
 /*    80 */   164,  164,  164,  164,  164,  164,  164,  164,  164,  164,
 /*    90 */   164,  164,  164,  164,  164,  164,  164,  164,  164,  164,
 /*   100 */   164,  164,  164,  164,  164,  164,  164,  164,  164,  164,
 /*   110 */   164,  164,  164,  164,  164,  164,  164,  164,  164,  164,
 /*   120 */   164,
};
#define YY_SZ_ACTTAB (int)(sizeof(yy_action)/sizeof(yy_action[0]))

/* The next table maps tokens into fallback tokens.  If a construct
** like the following:
** 
**      %fallback ID X Y Z.
**
** appears in the grammar, then ID becomes a fallback token for X, Y,
** and Z.  Whenever one of the tokens X, Y, or Z is input to the parser
** but it does not parse, the type of the token is changed to ID and
** the parse is retried before an error is thrown.
*/
#ifdef YYFALLBACK
static const YYCODETYPE yyFallback[] = {
};
#endif /* YYFALLBACK */

/* The following structure represents a single element of the
** parser's stack.  Information stored includes:
**
**   +  The state number for the parser at this level of the stack.
**
**   +  The value of the token stored at this level of the stack.
**      (In other words, the "major" token.)
**
**   +  The semantic value stored at this level of the stack.  This is
**      the information used by the action routines in the grammar.
**      It is sometimes called the "minor" token.
*/
struct yyStackEntry {
  YYACTIONTYPE stateno;  /* The state-number */
  YYCODETYPE major;      /* The major token value.  This is the code
                         ** number for the token at this stack level */
  YYMINORTYPE minor;     /* The user-supplied minor token value.  This
                         ** is the value of the token  */
};
typedef struct yyStackEntry yyStackEntry;

/* The state of the parser is completely contained in an instance of
** the following structure */
struct yyParser {
  int yyidx;                    /* Index of top element in stack */
#ifdef YYTRACKMAXSTACKDEPTH
  int yyidxMax;                 /* Maximum value of yyidx */
#endif
  int yyerrcnt;                 /* Shifts left before out of the error */
  ParseARG_SDECL                /* A place to hold %extra_argument */
#if YYSTACKDEPTH<=0
  int yystksz;                  /* Current side of the stack */
  yyStackEntry *yystack;        /* The parser's stack */
#else
  yyStackEntry yystack[YYSTACKDEPTH];  /* The parser's stack */
#endif
};
typedef struct yyParser yyParser;

#ifndef NDEBUG
#include <stdio.h>
static FILE *yyTraceFILE = 0;
static char *yyTracePrompt = 0;
#endif /* NDEBUG */

#ifndef NDEBUG
/* 
** Turn parser tracing on by giving a stream to which to write the trace
** and a prompt to preface each trace message.  Tracing is turned off
** by making either argument NULL 
**
** Inputs:
** <ul>
** <li> A FILE* to which trace output should be written.
**      If NULL, then tracing is turned off.
** <li> A prefix string written at the beginning of every
**      line of trace output.  If NULL, then tracing is
**      turned off.
** </ul>
**
** Outputs:
** None.
*/
void ParseTrace(FILE *TraceFILE, char *zTracePrompt){
  yyTraceFILE = TraceFILE;
  yyTracePrompt = zTracePrompt;
  if( yyTraceFILE==0 ) yyTracePrompt = 0;
  else if( yyTracePrompt==0 ) yyTraceFILE = 0;
}
#endif /* NDEBUG */

#ifndef NDEBUG
/* For tracing shifts, the names of all terminals and nonterminals
** are required.  The following table supplies these names */
static const char *const yyTokenName[] = { 
  "$",             "OR",            "AND",           "NOT",         
  "EQ",            "NE",            "LIKE",          "IS",          
  "IN",            "BETWEEN",       "GT",            "GE",          
  "LT",            "LE",            "PLUS",          "MINUS",       
  "STAR",          "SLASH",         "OPEN",          "CLOSE",       
  "COMMA",         "MOD",           "LOWER",         "UPPER",       
  "INITCAP",       "LENGTH",        "TO_CHAR",       "DATE_FORMAT", 
  "DECODE",        "NAME",          "STR",           "NUM",         
  "VARIABLE",      "NULL",          "ISNULL",        "NOW",         
  "SYSDATE",       "error",         "stmt",          "expr",        
  "value",         "list",        
};
#endif /* NDEBUG */

#ifndef NDEBUG
/* For tracing reduce actions, the names of all rules are required.
*/
static const char *const yyRuleName[] = {
 /*   0 */ "stmt ::= expr",
 /*   1 */ "expr ::= expr AND expr",
 /*   2 */ "expr ::= expr OR expr",
 /*   3 */ "expr ::= OPEN expr CLOSE",
 /*   4 */ "expr ::= NOT expr",
 /*   5 */ "expr ::= value LIKE value",
 /*   6 */ "expr ::= value NOT LIKE value",
 /*   7 */ "expr ::= value EQ value",
 /*   8 */ "expr ::= value NE value",
 /*   9 */ "expr ::= value GT value",
 /*  10 */ "expr ::= value GE value",
 /*  11 */ "expr ::= value LT value",
 /*  12 */ "expr ::= value LE value",
 /*  13 */ "value ::= value PLUS value",
 /*  14 */ "value ::= value MINUS value",
 /*  15 */ "value ::= value STAR value",
 /*  16 */ "value ::= value SLASH value",
 /*  17 */ "value ::= MINUS value",
 /*  18 */ "list ::= value",
 /*  19 */ "list ::= list COMMA value",
 /*  20 */ "expr ::= value IN OPEN list CLOSE",
 /*  21 */ "expr ::= value NOT IN OPEN list CLOSE",
 /*  22 */ "expr ::= value BETWEEN value AND value",
 /*  23 */ "expr ::= value NOT BETWEEN value AND value",
 /*  24 */ "value ::= OPEN value CLOSE",
 /*  25 */ "value ::= MOD OPEN value COMMA value CLOSE",
 /*  26 */ "value ::= LOWER OPEN value CLOSE",
 /*  27 */ "value ::= UPPER OPEN value CLOSE",
 /*  28 */ "value ::= INITCAP OPEN value CLOSE",
 /*  29 */ "value ::= LENGTH OPEN value CLOSE",
 /*  30 */ "value ::= TO_CHAR OPEN value COMMA value CLOSE",
 /*  31 */ "value ::= TO_CHAR OPEN value CLOSE",
 /*  32 */ "value ::= DATE_FORMAT OPEN value COMMA value CLOSE",
 /*  33 */ "value ::= DECODE OPEN list CLOSE",
 /*  34 */ "value ::= NAME OPEN list CLOSE",
 /*  35 */ "value ::= STR",
 /*  36 */ "value ::= NUM",
 /*  37 */ "value ::= VARIABLE",
 /*  38 */ "expr ::= VARIABLE IS NULL",
 /*  39 */ "expr ::= ISNULL OPEN VARIABLE CLOSE",
 /*  40 */ "expr ::= VARIABLE IS NOT NULL",
 /*  41 */ "value ::= NOW OPEN CLOSE",
 /*  42 */ "value ::= SYSDATE",
};
#endif /* NDEBUG */


#if YYSTACKDEPTH<=0
/*
** Try to increase the size of the parser stack.
*/
static void yyGrowStack(yyParser *p){
  int newSize;
  yyStackEntry *pNew;

  newSize = p->yystksz*2 + 100;
  pNew = realloc(p->yystack, newSize*sizeof(pNew[0]));
  if( pNew ){
    p->yystack = pNew;
    p->yystksz = newSize;
#ifndef NDEBUG
    if( yyTraceFILE ){
      fprintf(yyTraceFILE,"%sStack grows to %d entries!\n",
              yyTracePrompt, p->yystksz);
    }
#endif
  }
}
#endif

/* 
** This function allocates a new parser.
** The only argument is a pointer to a function which works like
** malloc.
**
** Inputs:
** A pointer to the function used to allocate memory.
**
** Outputs:
** A pointer to a parser.  This pointer is used in subsequent calls
** to Parse and ParseFree.
*/
void *ParseAlloc(void *(*mallocProc)(size_t)){
  yyParser *pParser;
  pParser = (yyParser*)(*mallocProc)( (size_t)sizeof(yyParser) );
  if( pParser ){
    pParser->yyidx = -1;
#ifdef YYTRACKMAXSTACKDEPTH
    pParser->yyidxMax = 0;
#endif
#if YYSTACKDEPTH<=0
    pParser->yystack = NULL;
    pParser->yystksz = 0;
    yyGrowStack(pParser);
#endif
  }
  return pParser;
}

/* The following function deletes the value associated with a
** symbol.  The symbol can be either a terminal or nonterminal.
** "yymajor" is the symbol code, and "yypminor" is a pointer to
** the value.
*/
static void yy_destructor(
  yyParser *yypParser,    /* The parser */
  YYCODETYPE yymajor,     /* Type code for object to destroy */
  YYMINORTYPE *yypminor   /* The object to be destroyed */
){
  ParseARG_FETCH;
  switch( yymajor ){
    /* Here is inserted the actions which take place when a
    ** terminal or non-terminal is destroyed.  This can happen
    ** when the symbol is popped from the stack during a
    ** reduce or during error processing or when a parser is 
    ** being destroyed before it is finished parsing.
    **
    ** Note: during a reduce, the only symbols destroyed are those
    ** which appear on the RHS of the rule, but which are not used
    ** inside the C code.
    */
      /* TERMINAL Destructor */
    case 1: /* OR */
    case 2: /* AND */
    case 3: /* NOT */
    case 4: /* EQ */
    case 5: /* NE */
    case 6: /* LIKE */
    case 7: /* IS */
    case 8: /* IN */
    case 9: /* BETWEEN */
    case 10: /* GT */
    case 11: /* GE */
    case 12: /* LT */
    case 13: /* LE */
    case 14: /* PLUS */
    case 15: /* MINUS */
    case 16: /* STAR */
    case 17: /* SLASH */
    case 18: /* OPEN */
    case 19: /* CLOSE */
    case 20: /* COMMA */
    case 21: /* MOD */
    case 22: /* LOWER */
    case 23: /* UPPER */
    case 24: /* INITCAP */
    case 25: /* LENGTH */
    case 26: /* TO_CHAR */
    case 27: /* DATE_FORMAT */
    case 28: /* DECODE */
    case 29: /* NAME */
    case 30: /* STR */
    case 31: /* NUM */
    case 32: /* VARIABLE */
    case 33: /* NULL */
    case 34: /* ISNULL */
    case 35: /* NOW */
    case 36: /* SYSDATE */
{
#line 98 "parse.y"
token_free ((yypminor->yy0));
#line 673 "parse.c"
}
      break;
      /* Default NON-TERMINAL Destructor */
    case 37: /* error */
    case 38: /* stmt */
    case 39: /* expr */
    case 40: /* value */
    case 41: /* list */
{
#line 95 "parse.y"
buffer_free ((yypminor->yy28));
#line 685 "parse.c"
}
      break;
    default:  break;   /* If no destructor action specified: do nothing */
  }
}

/*
** Pop the parser's stack once.
**
** If there is a destructor routine associated with the token which
** is popped from the stack, then call it.
**
** Return the major token number for the symbol popped.
*/
static int yy_pop_parser_stack(yyParser *pParser){
  YYCODETYPE yymajor;
  yyStackEntry *yytos = &pParser->yystack[pParser->yyidx];

  if( pParser->yyidx<0 ) return 0;
#ifndef NDEBUG
  if( yyTraceFILE && pParser->yyidx>=0 ){
    fprintf(yyTraceFILE,"%sPopping %s\n",
      yyTracePrompt,
      yyTokenName[yytos->major]);
  }
#endif
  yymajor = yytos->major;
  yy_destructor(pParser, yymajor, &yytos->minor);
  pParser->yyidx--;
  return yymajor;
}

/* 
** Deallocate and destroy a parser.  Destructors are all called for
** all stack elements before shutting the parser down.
**
** Inputs:
** <ul>
** <li>  A pointer to the parser.  This should be a pointer
**       obtained from ParseAlloc.
** <li>  A pointer to a function used to reclaim memory obtained
**       from malloc.
** </ul>
*/
void ParseFree(
  void *p,                    /* The parser to be deleted */
  void (*freeProc)(void*)     /* Function used to reclaim memory */
){
  yyParser *pParser = (yyParser*)p;
  if( pParser==0 ) return;
  while( pParser->yyidx>=0 ) yy_pop_parser_stack(pParser);
#if YYSTACKDEPTH<=0
  free(pParser->yystack);
#endif
  (*freeProc)((void*)pParser);
}

/*
** Return the peak depth of the stack for a parser.
*/
#ifdef YYTRACKMAXSTACKDEPTH
int ParseStackPeak(void *p){
  yyParser *pParser = (yyParser*)p;
  return pParser->yyidxMax;
}
#endif

/*
** Find the appropriate action for a parser given the terminal
** look-ahead token iLookAhead.
**
** If the look-ahead token is YYNOCODE, then check to see if the action is
** independent of the look-ahead.  If it is, return the action, otherwise
** return YY_NO_ACTION.
*/
static int yy_find_shift_action(
  yyParser *pParser,        /* The parser */
  YYCODETYPE iLookAhead     /* The look-ahead token */
){
  int i;
  int stateno = pParser->yystack[pParser->yyidx].stateno;
 
  if( stateno>YY_SHIFT_MAX || (i = yy_shift_ofst[stateno])==YY_SHIFT_USE_DFLT ){
    return yy_default[stateno];
  }
  assert( iLookAhead!=YYNOCODE );
  i += iLookAhead;
  if( i<0 || i>=YY_SZ_ACTTAB || yy_lookahead[i]!=iLookAhead ){
    if( iLookAhead>0 ){
#ifdef YYFALLBACK
      YYCODETYPE iFallback;            /* Fallback token */
      if( iLookAhead<sizeof(yyFallback)/sizeof(yyFallback[0])
             && (iFallback = yyFallback[iLookAhead])!=0 ){
#ifndef NDEBUG
        if( yyTraceFILE ){
          fprintf(yyTraceFILE, "%sFALLBACK %s => %s\n",
             yyTracePrompt, yyTokenName[iLookAhead], yyTokenName[iFallback]);
        }
#endif
        return yy_find_shift_action(pParser, iFallback);
      }
#endif
#ifdef YYWILDCARD
      {
        int j = i - iLookAhead + YYWILDCARD;
        if( j>=0 && j<YY_SZ_ACTTAB && yy_lookahead[j]==YYWILDCARD ){
#ifndef NDEBUG
          if( yyTraceFILE ){
            fprintf(yyTraceFILE, "%sWILDCARD %s => %s\n",
               yyTracePrompt, yyTokenName[iLookAhead], yyTokenName[YYWILDCARD]);
          }
#endif /* NDEBUG */
          return yy_action[j];
        }
      }
#endif /* YYWILDCARD */
    }
    return yy_default[stateno];
  }else{
    return yy_action[i];
  }
}

/*
** Find the appropriate action for a parser given the non-terminal
** look-ahead token iLookAhead.
**
** If the look-ahead token is YYNOCODE, then check to see if the action is
** independent of the look-ahead.  If it is, return the action, otherwise
** return YY_NO_ACTION.
*/
static int yy_find_reduce_action(
  int stateno,              /* Current state number */
  YYCODETYPE iLookAhead     /* The look-ahead token */
){
  int i;
#ifdef YYERRORSYMBOL
  if( stateno>YY_REDUCE_MAX ){
    return yy_default[stateno];
  }
#else
  assert( stateno<=YY_REDUCE_MAX );
#endif
  i = yy_reduce_ofst[stateno];
  assert( i!=YY_REDUCE_USE_DFLT );
  assert( iLookAhead!=YYNOCODE );
  i += iLookAhead;
#ifdef YYERRORSYMBOL
  if( i<0 || i>=YY_SZ_ACTTAB || yy_lookahead[i]!=iLookAhead ){
    return yy_default[stateno];
  }
#else
  assert( i>=0 && i<YY_SZ_ACTTAB );
  assert( yy_lookahead[i]==iLookAhead );
#endif
  return yy_action[i];
}

/*
** The following routine is called if the stack overflows.
*/
static void yyStackOverflow(yyParser *yypParser, YYMINORTYPE *yypMinor){
   ParseARG_FETCH;
   yypParser->yyidx--;
#ifndef NDEBUG
   if( yyTraceFILE ){
     fprintf(yyTraceFILE,"%sStack Overflow!\n",yyTracePrompt);
   }
#endif
   while( yypParser->yyidx>=0 ) yy_pop_parser_stack(yypParser);
   /* Here code is inserted which will execute if the parser
   ** stack every overflows */
#line 87 "parse.y"

	perr (priv, "Stackoverflow detected.");
#line 861 "parse.c"
   ParseARG_STORE; /* Suppress warning about unused %extra_argument var */
}

/*
** Perform a shift action.
*/
static void yy_shift(
  yyParser *yypParser,          /* The parser to be shifted */
  int yyNewState,               /* The new state to shift in */
  int yyMajor,                  /* The major token to shift in */
  YYMINORTYPE *yypMinor         /* Pointer to the minor token to shift in */
){
  yyStackEntry *yytos;
  yypParser->yyidx++;
#ifdef YYTRACKMAXSTACKDEPTH
  if( yypParser->yyidx>yypParser->yyidxMax ){
    yypParser->yyidxMax = yypParser->yyidx;
  }
#endif
#if YYSTACKDEPTH>0 
  if( yypParser->yyidx>=YYSTACKDEPTH ){
    yyStackOverflow(yypParser, yypMinor);
    return;
  }
#else
  if( yypParser->yyidx>=yypParser->yystksz ){
    yyGrowStack(yypParser);
    if( yypParser->yyidx>=yypParser->yystksz ){
      yyStackOverflow(yypParser, yypMinor);
      return;
    }
  }
#endif
  yytos = &yypParser->yystack[yypParser->yyidx];
  yytos->stateno = (YYACTIONTYPE)yyNewState;
  yytos->major = (YYCODETYPE)yyMajor;
  yytos->minor = *yypMinor;
#ifndef NDEBUG
  if( yyTraceFILE && yypParser->yyidx>0 ){
    int i;
    fprintf(yyTraceFILE,"%sShift %d\n",yyTracePrompt,yyNewState);
    fprintf(yyTraceFILE,"%sStack:",yyTracePrompt);
    for(i=1; i<=yypParser->yyidx; i++)
      fprintf(yyTraceFILE," %s",yyTokenName[yypParser->yystack[i].major]);
    fprintf(yyTraceFILE,"\n");
  }
#endif
}

/* The following table contains information about every rule that
** is used during the reduce.
*/
static const struct {
  YYCODETYPE lhs;         /* Symbol on the left-hand side of the rule */
  unsigned char nrhs;     /* Number of right-hand side symbols in the rule */
} yyRuleInfo[] = {
  { 38, 1 },
  { 39, 3 },
  { 39, 3 },
  { 39, 3 },
  { 39, 2 },
  { 39, 3 },
  { 39, 4 },
  { 39, 3 },
  { 39, 3 },
  { 39, 3 },
  { 39, 3 },
  { 39, 3 },
  { 39, 3 },
  { 40, 3 },
  { 40, 3 },
  { 40, 3 },
  { 40, 3 },
  { 40, 2 },
  { 41, 1 },
  { 41, 3 },
  { 39, 5 },
  { 39, 6 },
  { 39, 5 },
  { 39, 6 },
  { 40, 3 },
  { 40, 6 },
  { 40, 4 },
  { 40, 4 },
  { 40, 4 },
  { 40, 4 },
  { 40, 6 },
  { 40, 4 },
  { 40, 6 },
  { 40, 4 },
  { 40, 4 },
  { 40, 1 },
  { 40, 1 },
  { 40, 1 },
  { 39, 3 },
  { 39, 4 },
  { 39, 4 },
  { 40, 3 },
  { 40, 1 },
};

static void yy_accept(yyParser*);  /* Forward Declaration */

/*
** Perform a reduce action and the shift that must immediately
** follow the reduce.
*/
static void yy_reduce(
  yyParser *yypParser,         /* The parser */
  int yyruleno                 /* Number of the rule by which to reduce */
){
  int yygoto;                     /* The next state */
  int yyact;                      /* The next action */
  YYMINORTYPE yygotominor;        /* The LHS of the rule reduced */
  yyStackEntry *yymsp;            /* The top of the parser's stack */
  int yysize;                     /* Amount to pop the stack */
  ParseARG_FETCH;
  yymsp = &yypParser->yystack[yypParser->yyidx];
#ifndef NDEBUG
  if( yyTraceFILE && yyruleno>=0 
        && yyruleno<(int)(sizeof(yyRuleName)/sizeof(yyRuleName[0])) ){
    fprintf(yyTraceFILE, "%sReduce [%s].\n", yyTracePrompt,
      yyRuleName[yyruleno]);
  }
#endif /* NDEBUG */

  /* Silence complaints from purify about yygotominor being uninitialized
  ** in some cases when it is copied into the stack after the following
  ** switch.  yygotominor is uninitialized when a rule reduces that does
  ** not set the value of its left-hand side nonterminal.  Leaving the
  ** value of the nonterminal uninitialized is utterly harmless as long
  ** as the value is never used.  So really the only thing this code
  ** accomplishes is to quieten purify.  
  **
  ** 2007-01-16:  The wireshark project (www.wireshark.org) reports that
  ** without this code, their parser segfaults.  I'm not sure what there
  ** parser is doing to make this happen.  This is the second bug report
  ** from wireshark this week.  Clearly they are stressing Lemon in ways
  ** that it has not been previously stressed...  (SQLite ticket #2172)
  */
  /*memset(&yygotominor, 0, sizeof(yygotominor));*/
  yygotominor = yyzerominor;


  switch( yyruleno ){
  /* Beginning here are the reduction cases.  A typical example
  ** follows:
  **   case 0:
  **  #line <lineno> <grammarfile>
  **     { ... }           // User supplied code
  **  #line <lineno> <thisfile>
  **     break;
  */
      case 0: /* stmt ::= expr */
#line 109 "parse.y"
{
	buffer_appendbuf (priv -> buf, yymsp[0].minor.yy28);
	buffer_free (yymsp[0].minor.yy28);
}
#line 1021 "parse.c"
        break;
      case 1: /* expr ::= expr AND expr */
#line 113 "parse.y"
{
	yygotominor.yy28 = buffer_alloc (yymsp[-2].minor.yy28 -> length + yymsp[0].minor.yy28 -> length + 12);
	buffer_format (yygotominor.yy28, "(%s) and (%s)", buffer_string (yymsp[-2].minor.yy28), buffer_string (yymsp[0].minor.yy28));
	buffer_free (yymsp[-2].minor.yy28);
	buffer_free (yymsp[0].minor.yy28);
  yy_destructor(yypParser,2,&yymsp[-1].minor);
}
#line 1032 "parse.c"
        break;
      case 2: /* expr ::= expr OR expr */
#line 119 "parse.y"
{
	yygotominor.yy28 = buffer_alloc (yymsp[-2].minor.yy28 -> length + yymsp[0].minor.yy28 -> length + 12);
	buffer_format (yygotominor.yy28, "(%s) or (%s)", buffer_string (yymsp[-2].minor.yy28), buffer_string (yymsp[0].minor.yy28));
	buffer_free (yymsp[-2].minor.yy28);
	buffer_free (yymsp[0].minor.yy28);
  yy_destructor(yypParser,1,&yymsp[-1].minor);
}
#line 1043 "parse.c"
        break;
      case 3: /* expr ::= OPEN expr CLOSE */
#line 125 "parse.y"
{
	yygotominor.yy28 = buffer_alloc (yymsp[-1].minor.yy28 -> length + 6);
	buffer_format (yygotominor.yy28, "(%s)", buffer_string (yymsp[-1].minor.yy28));
	buffer_free (yymsp[-1].minor.yy28);
  yy_destructor(yypParser,18,&yymsp[-2].minor);
  yy_destructor(yypParser,19,&yymsp[0].minor);
}
#line 1054 "parse.c"
        break;
      case 4: /* expr ::= NOT expr */
#line 130 "parse.y"
{
	yygotominor.yy28 = buffer_alloc (yymsp[0].minor.yy28 -> length + 8);
	buffer_format (yygotominor.yy28, "not (%s)", buffer_string (yymsp[0].minor.yy28));
	buffer_free (yymsp[0].minor.yy28);
  yy_destructor(yypParser,3,&yymsp[-1].minor);
}
#line 1064 "parse.c"
        break;
      case 5: /* expr ::= value LIKE value */
#line 135 "parse.y"
{
	yygotominor.yy28 = buffer_alloc (yymsp[-2].minor.yy28 -> length + yymsp[0].minor.yy28 -> length + 20);
	buffer_format (yygotominor.yy28, "like (%s, %s)", buffer_string (yymsp[-2].minor.yy28), buffer_string (yymsp[0].minor.yy28));
	buffer_free (yymsp[-2].minor.yy28);
	buffer_free (yymsp[0].minor.yy28);
  yy_destructor(yypParser,6,&yymsp[-1].minor);
}
#line 1075 "parse.c"
        break;
      case 6: /* expr ::= value NOT LIKE value */
#line 141 "parse.y"
{
	yygotominor.yy28 = buffer_alloc (yymsp[-3].minor.yy28 -> length + yymsp[0].minor.yy28 -> length + 32);
	buffer_format (yygotominor.yy28, "(not like (%s, %s))", buffer_string (yymsp[-3].minor.yy28), buffer_string (yymsp[0].minor.yy28));
	buffer_free (yymsp[-3].minor.yy28);
	buffer_free (yymsp[0].minor.yy28);
  yy_destructor(yypParser,3,&yymsp[-2].minor);
  yy_destructor(yypParser,6,&yymsp[-1].minor);
}
#line 1087 "parse.c"
        break;
      case 7: /* expr ::= value EQ value */
#line 147 "parse.y"
{
	yygotominor.yy28 = operation (yymsp[-2].minor.yy28, "==", yymsp[0].minor.yy28);
  yy_destructor(yypParser,4,&yymsp[-1].minor);
}
#line 1095 "parse.c"
        break;
      case 8: /* expr ::= value NE value */
#line 150 "parse.y"
{
	yygotominor.yy28 = operation (yymsp[-2].minor.yy28, "!=", yymsp[0].minor.yy28);
  yy_destructor(yypParser,5,&yymsp[-1].minor);
}
#line 1103 "parse.c"
        break;
      case 9: /* expr ::= value GT value */
#line 153 "parse.y"
{
	yygotominor.yy28 = operation (yymsp[-2].minor.yy28, ">", yymsp[0].minor.yy28);
  yy_destructor(yypParser,10,&yymsp[-1].minor);
}
#line 1111 "parse.c"
        break;
      case 10: /* expr ::= value GE value */
#line 156 "parse.y"
{
	yygotominor.yy28 = operation (yymsp[-2].minor.yy28, ">=", yymsp[0].minor.yy28);
  yy_destructor(yypParser,11,&yymsp[-1].minor);
}
#line 1119 "parse.c"
        break;
      case 11: /* expr ::= value LT value */
#line 159 "parse.y"
{
	yygotominor.yy28 = operation (yymsp[-2].minor.yy28, "<", yymsp[0].minor.yy28);
  yy_destructor(yypParser,12,&yymsp[-1].minor);
}
#line 1127 "parse.c"
        break;
      case 12: /* expr ::= value LE value */
#line 162 "parse.y"
{
	yygotominor.yy28 = operation (yymsp[-2].minor.yy28, "<=", yymsp[0].minor.yy28);
  yy_destructor(yypParser,13,&yymsp[-1].minor);
}
#line 1135 "parse.c"
        break;
      case 13: /* value ::= value PLUS value */
#line 165 "parse.y"
{
	yygotominor.yy28 = operation (yymsp[-2].minor.yy28, "+", yymsp[0].minor.yy28);
  yy_destructor(yypParser,14,&yymsp[-1].minor);
}
#line 1143 "parse.c"
        break;
      case 14: /* value ::= value MINUS value */
#line 168 "parse.y"
{
	yygotominor.yy28 = operation (yymsp[-2].minor.yy28, "-", yymsp[0].minor.yy28);
  yy_destructor(yypParser,15,&yymsp[-1].minor);
}
#line 1151 "parse.c"
        break;
      case 15: /* value ::= value STAR value */
#line 171 "parse.y"
{
	yygotominor.yy28 = operation (yymsp[-2].minor.yy28, "*", yymsp[0].minor.yy28);
  yy_destructor(yypParser,16,&yymsp[-1].minor);
}
#line 1159 "parse.c"
        break;
      case 16: /* value ::= value SLASH value */
#line 174 "parse.y"
{
	yygotominor.yy28 = operation (yymsp[-2].minor.yy28, "/", yymsp[0].minor.yy28);
  yy_destructor(yypParser,17,&yymsp[-1].minor);
}
#line 1167 "parse.c"
        break;
      case 17: /* value ::= MINUS value */
#line 177 "parse.y"
{
	yygotominor.yy28 = buffer_alloc (yymsp[0].minor.yy28 -> length + 2);
	buffer_format (yygotominor.yy28, "-%s", buffer_string (yymsp[0].minor.yy28));
	buffer_free (yymsp[0].minor.yy28);
  yy_destructor(yypParser,15,&yymsp[-1].minor);
}
#line 1177 "parse.c"
        break;
      case 18: /* list ::= value */
#line 182 "parse.y"
{
	yygotominor.yy28 = yymsp[0].minor.yy28;
}
#line 1184 "parse.c"
        break;
      case 19: /* list ::= list COMMA value */
#line 185 "parse.y"
{
	yygotominor.yy28 = buffer_alloc (yymsp[-2].minor.yy28 -> length + yymsp[0].minor.yy28 -> length + 8);
	buffer_format (yygotominor.yy28, "%s, %s", buffer_string (yymsp[-2].minor.yy28), buffer_string (yymsp[0].minor.yy28));
	buffer_free (yymsp[-2].minor.yy28);
	buffer_free (yymsp[0].minor.yy28);
  yy_destructor(yypParser,20,&yymsp[-1].minor);
}
#line 1195 "parse.c"
        break;
      case 20: /* expr ::= value IN OPEN list CLOSE */
#line 191 "parse.y"
{
	yygotominor.yy28 = buffer_alloc (yymsp[-4].minor.yy28 -> length + yymsp[-1].minor.yy28 -> length + 16);
	buffer_format (yygotominor.yy28, "in (%s, %s)", buffer_string (yymsp[-4].minor.yy28), buffer_string (yymsp[-1].minor.yy28));
	buffer_free (yymsp[-4].minor.yy28);
	buffer_free (yymsp[-1].minor.yy28);
  yy_destructor(yypParser,8,&yymsp[-3].minor);
  yy_destructor(yypParser,18,&yymsp[-2].minor);
  yy_destructor(yypParser,19,&yymsp[0].minor);
}
#line 1208 "parse.c"
        break;
      case 21: /* expr ::= value NOT IN OPEN list CLOSE */
#line 197 "parse.y"
{
	yygotominor.yy28 = buffer_alloc (yymsp[-5].minor.yy28 -> length + yymsp[-1].minor.yy28 -> length + 24);
	buffer_format (yygotominor.yy28, "(not in (%s, %s))", buffer_string (yymsp[-5].minor.yy28), buffer_string (yymsp[-1].minor.yy28));
	buffer_free (yymsp[-5].minor.yy28);
	buffer_free (yymsp[-1].minor.yy28);
  yy_destructor(yypParser,3,&yymsp[-4].minor);
  yy_destructor(yypParser,8,&yymsp[-3].minor);
  yy_destructor(yypParser,18,&yymsp[-2].minor);
  yy_destructor(yypParser,19,&yymsp[0].minor);
}
#line 1222 "parse.c"
        break;
      case 22: /* expr ::= value BETWEEN value AND value */
#line 203 "parse.y"
{
	yygotominor.yy28 = buffer_alloc (yymsp[-4].minor.yy28 -> length + yymsp[-2].minor.yy28 -> length + yymsp[0].minor.yy28 -> length + 32);
	buffer_format (yygotominor.yy28, "between (%s, %s, %s)", buffer_string (yymsp[-4].minor.yy28), buffer_string (yymsp[-2].minor.yy28), buffer_string (yymsp[0].minor.yy28));
	buffer_free (yymsp[-4].minor.yy28);
	buffer_free (yymsp[-2].minor.yy28);
	buffer_free (yymsp[0].minor.yy28);
  yy_destructor(yypParser,9,&yymsp[-3].minor);
  yy_destructor(yypParser,2,&yymsp[-1].minor);
}
#line 1235 "parse.c"
        break;
      case 23: /* expr ::= value NOT BETWEEN value AND value */
#line 210 "parse.y"
{
	yygotominor.yy28 = buffer_alloc (yymsp[-5].minor.yy28 -> length + yymsp[-2].minor.yy28 -> length + yymsp[0].minor.yy28 -> length + 32);
	buffer_format (yygotominor.yy28, "(not between (%s, %s, %s))", buffer_string (yymsp[-5].minor.yy28), buffer_string (yymsp[-2].minor.yy28), buffer_string (yymsp[0].minor.yy28));
	buffer_free (yymsp[-5].minor.yy28);
	buffer_free (yymsp[-2].minor.yy28);
	buffer_free (yymsp[0].minor.yy28);
  yy_destructor(yypParser,3,&yymsp[-4].minor);
  yy_destructor(yypParser,9,&yymsp[-3].minor);
  yy_destructor(yypParser,2,&yymsp[-1].minor);
}
#line 1249 "parse.c"
        break;
      case 24: /* value ::= OPEN value CLOSE */
#line 217 "parse.y"
{
	yygotominor.yy28 = buffer_alloc (yymsp[-1].minor.yy28 -> length + 4);
	buffer_format (yygotominor.yy28, "(%s)", buffer_string (yymsp[-1].minor.yy28));
	buffer_free (yymsp[-1].minor.yy28);
  yy_destructor(yypParser,18,&yymsp[-2].minor);
  yy_destructor(yypParser,19,&yymsp[0].minor);
}
#line 1260 "parse.c"
        break;
      case 25: /* value ::= MOD OPEN value COMMA value CLOSE */
#line 222 "parse.y"
{
	yygotominor.yy28 = buffer_alloc (yymsp[-3].minor.yy28 -> length + yymsp[-1].minor.yy28 -> length + 64);
	buffer_format (yygotominor.yy28, "modulo (%s, %s)", buffer_string (yymsp[-3].minor.yy28), buffer_string (yymsp[-1].minor.yy28));
	buffer_free (yymsp[-3].minor.yy28);
	buffer_free (yymsp[-1].minor.yy28);
  yy_destructor(yypParser,21,&yymsp[-5].minor);
  yy_destructor(yypParser,18,&yymsp[-4].minor);
  yy_destructor(yypParser,20,&yymsp[-2].minor);
  yy_destructor(yypParser,19,&yymsp[0].minor);
}
#line 1274 "parse.c"
        break;
      case 26: /* value ::= LOWER OPEN value CLOSE */
#line 228 "parse.y"
{
	yygotominor.yy28 = conversion ("lower", yymsp[-1].minor.yy28, priv -> xconv, xconv_lower);
  yy_destructor(yypParser,22,&yymsp[-3].minor);
  yy_destructor(yypParser,18,&yymsp[-2].minor);
  yy_destructor(yypParser,19,&yymsp[0].minor);
}
#line 1284 "parse.c"
        break;
      case 27: /* value ::= UPPER OPEN value CLOSE */
#line 231 "parse.y"
{
	yygotominor.yy28 = conversion ("upper", yymsp[-1].minor.yy28, priv -> xconv, xconv_upper);
  yy_destructor(yypParser,23,&yymsp[-3].minor);
  yy_destructor(yypParser,18,&yymsp[-2].minor);
  yy_destructor(yypParser,19,&yymsp[0].minor);
}
#line 1294 "parse.c"
        break;
      case 28: /* value ::= INITCAP OPEN value CLOSE */
#line 234 "parse.y"
{
	yygotominor.yy28 = conversion ("captialize", yymsp[-1].minor.yy28, priv -> xconv, xconv_title);
  yy_destructor(yypParser,24,&yymsp[-3].minor);
  yy_destructor(yypParser,18,&yymsp[-2].minor);
  yy_destructor(yypParser,19,&yymsp[0].minor);
}
#line 1304 "parse.c"
        break;
      case 29: /* value ::= LENGTH OPEN value CLOSE */
#line 237 "parse.y"
{
	yygotominor.yy28 = buffer_alloc (yymsp[-1].minor.yy28 -> length + 16);
	buffer_format (yygotominor.yy28, "length (%s)", buffer_string (yymsp[-1].minor.yy28));
	buffer_free (yymsp[-1].minor.yy28);
  yy_destructor(yypParser,25,&yymsp[-3].minor);
  yy_destructor(yypParser,18,&yymsp[-2].minor);
  yy_destructor(yypParser,19,&yymsp[0].minor);
}
#line 1316 "parse.c"
        break;
      case 30: /* value ::= TO_CHAR OPEN value COMMA value CLOSE */
#line 242 "parse.y"
{
	yygotominor.yy28 = buffer_alloc (yymsp[-3].minor.yy28 -> length + yymsp[-1].minor.yy28 -> length + 64);
	buffer_format (yygotominor.yy28, "to_char (%s, %s)", buffer_string (yymsp[-3].minor.yy28), buffer_string (yymsp[-1].minor.yy28));
	buffer_free (yymsp[-3].minor.yy28);
	buffer_free (yymsp[-1].minor.yy28);
  yy_destructor(yypParser,26,&yymsp[-5].minor);
  yy_destructor(yypParser,18,&yymsp[-4].minor);
  yy_destructor(yypParser,20,&yymsp[-2].minor);
  yy_destructor(yypParser,19,&yymsp[0].minor);
}
#line 1330 "parse.c"
        break;
      case 31: /* value ::= TO_CHAR OPEN value CLOSE */
#line 248 "parse.y"
{
	yygotominor.yy28 = buffer_alloc (yymsp[-1].minor.yy28 -> length + 64);
	buffer_format (yygotominor.yy28, "string (%s)", buffer_string (yymsp[-1].minor.yy28));
	buffer_free (yymsp[-1].minor.yy28);
  yy_destructor(yypParser,26,&yymsp[-3].minor);
  yy_destructor(yypParser,18,&yymsp[-2].minor);
  yy_destructor(yypParser,19,&yymsp[0].minor);
}
#line 1342 "parse.c"
        break;
      case 32: /* value ::= DATE_FORMAT OPEN value COMMA value CLOSE */
#line 253 "parse.y"
{
	yygotominor.yy28 = buffer_alloc (yymsp[-3].minor.yy28 -> length + yymsp[-1].minor.yy28 -> length + 64);
	buffer_format (yygotominor.yy28, "date_format (%s, %s)", buffer_string (yymsp[-3].minor.yy28), buffer_string (yymsp[-1].minor.yy28));
	buffer_free (yymsp[-3].minor.yy28);
	buffer_free (yymsp[-1].minor.yy28);
  yy_destructor(yypParser,27,&yymsp[-5].minor);
  yy_destructor(yypParser,18,&yymsp[-4].minor);
  yy_destructor(yypParser,20,&yymsp[-2].minor);
  yy_destructor(yypParser,19,&yymsp[0].minor);
}
#line 1356 "parse.c"
        break;
      case 33: /* value ::= DECODE OPEN list CLOSE */
#line 259 "parse.y"
{
	yygotominor.yy28 = buffer_alloc (yymsp[-1].minor.yy28 -> length + 24);
	buffer_format (yygotominor.yy28, "decode (%s)", buffer_string (yymsp[-1].minor.yy28));
	buffer_free (yymsp[-1].minor.yy28);
  yy_destructor(yypParser,28,&yymsp[-3].minor);
  yy_destructor(yypParser,18,&yymsp[-2].minor);
  yy_destructor(yypParser,19,&yymsp[0].minor);
}
#line 1368 "parse.c"
        break;
      case 34: /* value ::= NAME OPEN list CLOSE */
#line 264 "parse.y"
{
	yygotominor.yy28 = buffer_alloc (yymsp[-1].minor.yy28 -> length + 256);
	buffer_format (yygotominor.yy28, "custom->%s (%s)", yymsp[-3].minor.yy0 -> token, buffer_string (yymsp[-1].minor.yy28));
	token_free (yymsp[-3].minor.yy0);
	buffer_free (yymsp[-1].minor.yy28);
  yy_destructor(yypParser,18,&yymsp[-2].minor);
  yy_destructor(yypParser,19,&yymsp[0].minor);
}
#line 1380 "parse.c"
        break;
      case 35: /* value ::= STR */
      case 36: /* value ::= NUM */
      case 37: /* value ::= VARIABLE */
#line 270 "parse.y"
{
	yygotominor.yy28 = tok2buf (yymsp[0].minor.yy0);
}
#line 1389 "parse.c"
        break;
      case 38: /* expr ::= VARIABLE IS NULL */
#line 279 "parse.y"
{
	yygotominor.yy28 = buffer_alloc (256);
	buffer_format (yygotominor.yy28, "NULL$%s", yymsp[-2].minor.yy0 -> token);
	token_free (yymsp[-2].minor.yy0);
  yy_destructor(yypParser,7,&yymsp[-1].minor);
  yy_destructor(yypParser,33,&yymsp[0].minor);
}
#line 1400 "parse.c"
        break;
      case 39: /* expr ::= ISNULL OPEN VARIABLE CLOSE */
#line 284 "parse.y"
{
	yygotominor.yy28 = buffer_alloc (256);
	buffer_format (yygotominor.yy28, "NULL$%s", yymsp[-1].minor.yy0 -> token);
	token_free (yymsp[-1].minor.yy0);
  yy_destructor(yypParser,34,&yymsp[-3].minor);
  yy_destructor(yypParser,18,&yymsp[-2].minor);
  yy_destructor(yypParser,19,&yymsp[0].minor);
}
#line 1412 "parse.c"
        break;
      case 40: /* expr ::= VARIABLE IS NOT NULL */
#line 289 "parse.y"
{
	yygotominor.yy28 = buffer_alloc (256);
	buffer_format (yygotominor.yy28, "(not NULL$%s)", yymsp[-3].minor.yy0 -> token);
	token_free (yymsp[-3].minor.yy0);
  yy_destructor(yypParser,7,&yymsp[-2].minor);
  yy_destructor(yypParser,3,&yymsp[-1].minor);
  yy_destructor(yypParser,33,&yymsp[0].minor);
}
#line 1424 "parse.c"
        break;
      case 41: /* value ::= NOW OPEN CLOSE */
#line 294 "parse.y"
{
	yygotominor.yy28 = buffer_alloc (16);
	buffer_sets (yygotominor.yy28, "sysdate");
  yy_destructor(yypParser,35,&yymsp[-2].minor);
  yy_destructor(yypParser,18,&yymsp[-1].minor);
  yy_destructor(yypParser,19,&yymsp[0].minor);
}
#line 1435 "parse.c"
        break;
      case 42: /* value ::= SYSDATE */
#line 298 "parse.y"
{
	yygotominor.yy28 = buffer_alloc (16);
	buffer_sets (yygotominor.yy28, "sysdate");
  yy_destructor(yypParser,36,&yymsp[0].minor);
}
#line 1444 "parse.c"
        break;
  };
  yygoto = yyRuleInfo[yyruleno].lhs;
  yysize = yyRuleInfo[yyruleno].nrhs;
  yypParser->yyidx -= yysize;
  yyact = yy_find_reduce_action(yymsp[-yysize].stateno,(YYCODETYPE)yygoto);
  if( yyact < YYNSTATE ){
#ifdef NDEBUG
    /* If we are not debugging and the reduce action popped at least
    ** one element off the stack, then we can push the new element back
    ** onto the stack here, and skip the stack overflow test in yy_shift().
    ** That gives a significant speed improvement. */
    if( yysize ){
      yypParser->yyidx++;
      yymsp -= yysize-1;
      yymsp->stateno = yyact;
      yymsp->major = yygoto;
      yymsp->minor = yygotominor;
    }else
#endif
    {
      yy_shift(yypParser,yyact,yygoto,&yygotominor);
    }
  }else{
    assert( yyact == YYNSTATE + YYNRULE + 1 );
    yy_accept(yypParser);
  }
}

/*
** The following code executes when the parse fails
*/
static void yy_parse_failed(
  yyParser *yypParser           /* The parser */
){
  ParseARG_FETCH;
#ifndef NDEBUG
  if( yyTraceFILE ){
    fprintf(yyTraceFILE,"%sFail!\n",yyTracePrompt);
  }
#endif
  while( yypParser->yyidx>=0 ) yy_pop_parser_stack(yypParser);
  /* Here code is inserted which will be executed whenever the
  ** parser fails */
#line 84 "parse.y"

	perr (priv, "Failed in parsing.");
#line 1492 "parse.c"
  ParseARG_STORE; /* Suppress warning about unused %extra_argument variable */
}

/*
** The following code executes when a syntax error first occurs.
*/
static void yy_syntax_error(
  yyParser *yypParser,           /* The parser */
  int yymajor,                   /* The major type of the error token */
  YYMINORTYPE yyminor            /* The minor type of the error token */
){
  ParseARG_FETCH;
#define TOKEN (yyminor.yy0)
#line 90 "parse.y"

	perr (priv, "Syntax error hit.");
#line 1509 "parse.c"
  ParseARG_STORE; /* Suppress warning about unused %extra_argument variable */
}

/*
** The following is executed when the parser accepts
*/
static void yy_accept(
  yyParser *yypParser           /* The parser */
){
  ParseARG_FETCH;
#ifndef NDEBUG
  if( yyTraceFILE ){
    fprintf(yyTraceFILE,"%sAccept!\n",yyTracePrompt);
  }
#endif
  while( yypParser->yyidx>=0 ) yy_pop_parser_stack(yypParser);
  /* Here code is inserted which will be executed whenever the
  ** parser accepts */
  ParseARG_STORE; /* Suppress warning about unused %extra_argument variable */
}

/* The main parser program.
** The first argument is a pointer to a structure obtained from
** "ParseAlloc" which describes the current state of the parser.
** The second argument is the major token number.  The third is
** the minor token.  The fourth optional argument is whatever the
** user wants (and specified in the grammar) and is available for
** use by the action routines.
**
** Inputs:
** <ul>
** <li> A pointer to the parser (an opaque structure.)
** <li> The major token number.
** <li> The minor token number.
** <li> An option argument of a grammar-specified type.
** </ul>
**
** Outputs:
** None.
*/
void Parse(
  void *yyp,                   /* The parser */
  int yymajor,                 /* The major token code number */
  ParseTOKENTYPE yyminor       /* The value for the token */
  ParseARG_PDECL               /* Optional %extra_argument parameter */
){
  YYMINORTYPE yyminorunion;
  int yyact;            /* The parser action. */
  int yyendofinput;     /* True if we are at the end of input */
#ifdef YYERRORSYMBOL
  int yyerrorhit = 0;   /* True if yymajor has invoked an error */
#endif
  yyParser *yypParser;  /* The parser */

  /* (re)initialize the parser, if necessary */
  yypParser = (yyParser*)yyp;
  if( yypParser->yyidx<0 ){
#if YYSTACKDEPTH<=0
    if( yypParser->yystksz <=0 ){
      /*memset(&yyminorunion, 0, sizeof(yyminorunion));*/
      yyminorunion = yyzerominor;
      yyStackOverflow(yypParser, &yyminorunion);
      return;
    }
#endif
    yypParser->yyidx = 0;
    yypParser->yyerrcnt = -1;
    yypParser->yystack[0].stateno = 0;
    yypParser->yystack[0].major = 0;
  }
  yyminorunion.yy0 = yyminor;
  yyendofinput = (yymajor==0);
  ParseARG_STORE;

#ifndef NDEBUG
  if( yyTraceFILE ){
    fprintf(yyTraceFILE,"%sInput %s\n",yyTracePrompt,yyTokenName[yymajor]);
  }
#endif

  do{
    yyact = yy_find_shift_action(yypParser,(YYCODETYPE)yymajor);
    if( yyact<YYNSTATE ){
      assert( !yyendofinput );  /* Impossible to shift the $ token */
      yy_shift(yypParser,yyact,yymajor,&yyminorunion);
      yypParser->yyerrcnt--;
      yymajor = YYNOCODE;
    }else if( yyact < YYNSTATE + YYNRULE ){
      yy_reduce(yypParser,yyact-YYNSTATE);
    }else{
      assert( yyact == YY_ERROR_ACTION );
#ifdef YYERRORSYMBOL
      int yymx;
#endif
#ifndef NDEBUG
      if( yyTraceFILE ){
        fprintf(yyTraceFILE,"%sSyntax Error!\n",yyTracePrompt);
      }
#endif
#ifdef YYERRORSYMBOL
      /* A syntax error has occurred.
      ** The response to an error depends upon whether or not the
      ** grammar defines an error token "ERROR".  
      **
      ** This is what we do if the grammar does define ERROR:
      **
      **  * Call the %syntax_error function.
      **
      **  * Begin popping the stack until we enter a state where
      **    it is legal to shift the error symbol, then shift
      **    the error symbol.
      **
      **  * Set the error count to three.
      **
      **  * Begin accepting and shifting new tokens.  No new error
      **    processing will occur until three tokens have been
      **    shifted successfully.
      **
      */
      if( yypParser->yyerrcnt<0 ){
        yy_syntax_error(yypParser,yymajor,yyminorunion);
      }
      yymx = yypParser->yystack[yypParser->yyidx].major;
      if( yymx==YYERRORSYMBOL || yyerrorhit ){
#ifndef NDEBUG
        if( yyTraceFILE ){
          fprintf(yyTraceFILE,"%sDiscard input token %s\n",
             yyTracePrompt,yyTokenName[yymajor]);
        }
#endif
        yy_destructor(yypParser, (YYCODETYPE)yymajor,&yyminorunion);
        yymajor = YYNOCODE;
      }else{
         while(
          yypParser->yyidx >= 0 &&
          yymx != YYERRORSYMBOL &&
          (yyact = yy_find_reduce_action(
                        yypParser->yystack[yypParser->yyidx].stateno,
                        YYERRORSYMBOL)) >= YYNSTATE
        ){
          yy_pop_parser_stack(yypParser);
        }
        if( yypParser->yyidx < 0 || yymajor==0 ){
          yy_destructor(yypParser,(YYCODETYPE)yymajor,&yyminorunion);
          yy_parse_failed(yypParser);
          yymajor = YYNOCODE;
        }else if( yymx!=YYERRORSYMBOL ){
          YYMINORTYPE u2;
          u2.YYERRSYMDT = 0;
          yy_shift(yypParser,yyact,YYERRORSYMBOL,&u2);
        }
      }
      yypParser->yyerrcnt = 3;
      yyerrorhit = 1;
#else  /* YYERRORSYMBOL is not defined */
      /* This is what we do if the grammar does not define ERROR:
      **
      **  * Report an error message, and throw away the input token.
      **
      **  * If the input token is $, then fail the parse.
      **
      ** As before, subsequent error messages are suppressed until
      ** three input tokens have been successfully shifted.
      */
      if( yypParser->yyerrcnt<=0 ){
        yy_syntax_error(yypParser,yymajor,yyminorunion);
      }
      yypParser->yyerrcnt = 3;
      yy_destructor(yypParser,(YYCODETYPE)yymajor,&yyminorunion);
      if( yyendofinput ){
        yy_parse_failed(yypParser);
      }
      yymajor = YYNOCODE;
#endif
    }
  }while( yymajor!=YYNOCODE && yypParser->yyidx>=0 );
  return;
}
