   _        ___  ____ ____  ____
  |_|_ _   / _ \/ ___/ ___||  _ \   ___  __ _
  _|_||_| | | | \___ \___ \| |_) | / __|/ _` |
 |_||_|_| | |_| |___) |__) |  __/  \__ \ (_| |
  |_|_|_|  \___/|____/____/|_|     |___/\__,_|

  OSSP sa - Socket Abstraction

  TODO

  The following items still have to be done:

  -NONE-

  CANDO

  The following items can be done if wished and time permits:

  o Add support for more socket options, for instance SO_BROADCAST for
    enabling permission to transmit broadcast messages.

  o what about actually written bytes in case of sa_writef?
    Should we internally retry somehow?

  o provide Perl glue module SA.

  o provide "satool", a command line interface to the OSSP sa API.
    Can optionally use readline or libedit, etc.

  o support for SSL/TLS via small abstraction layer on top of OpenSSL and
    gnutls libraries.

  o Consistently use PF_XXX instead of AF_XXX whenever not directly
    address related things are done in order to even closer conform to POSIX.

  o DNS Resolving and multiple results.

    On DNS resolving name, multiple result addresses can be returned.  Either
    because of Round-Robin entries, multi-homed hosts or because of hosts
    talking both IPv4 and IPv6, etc. Each address has to be tried on
    socket+connect. What to do? For some hints see:
    http://www.v6.wide.ad.jp/Presentations/ai3-penang0010-v6programming/mgp00015.html

  o Nagle's Algorithm and Flushing of Output Buffers.

    The kernel performs Nagle's Algorithm (see RFC 896 and search for "nagle
    algorithm" on www.whatis.com [currently
    http://searchnetworking.techtarget.com/sDefinition/0,,sid7_gci754347,00.html]
    on his internal output buffers. Although we flush our user-space output
    buffers only if we really perform a read(2) (and not if the read is going to
    be served entirely from the local buffer) this does not mean that really
    every character the remote has already sent is also already read by us.
    Because the kernel also has a read buffer. Optimal behaviour would be that
    we flush out output buffer only if the read(2) would block. OTOH flushing
    out buffers means performing a write(2) and this again is buffered in the
    kernel, too. So performing an optimal read->write->read->write->... sequence
    is very complex and non-trivial to implement. Especially because even using
    Nagle's Algorithm always is not the right choice (see
    http://www.w3.org/Protocols/HTTP/Performance/Nagle/ for details), especially
    when it comes to pipelining protocols.

  o Support for writev(2).

    This can be done by internally switching to always use writev(2),
    providing an emulation for writev(2) ala Pth and by basing the write
    calls always on writev.

  o Kernel Read/Write Buffer Adjustments.

    BSD Sockets usually provide (see setsockopt(2)):

        SO_SNDBUF          set buffer size for output
        SO_RCVBUF          set buffer size for input
        SO_SNDLOWAT        set minimum count for output
        SO_RCVLOWAT        set minimum count for input

    This would mean that we could also allow the control
    of the kernel buffers via

        SA_BUFFER_KREAD
        SA_BUFFER_KWRITE

    Unfortunately the whole kernel buffer issue is very complex, because
    according to STEVENS there are both minimum and maximum sizes and both
    borders heavily depend on the currently used protocol (TCP or UDP) and
    the MTU of the underlying network, etc. This all together seems like
    opening a can of worms if we provide SA_BUFFER_K{READ,WRITE}.

