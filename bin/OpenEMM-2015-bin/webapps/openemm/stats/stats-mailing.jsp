<%@ page language="java" contentType="text/html; charset=utf-8"
         import="org.agnitas.beans.TrackableLink, org.agnitas.stat.MailingStatEntry, org.agnitas.beans.Mailing, org.agnitas.stat.DeliveryStat, org.agnitas.stat.URLStatEntry, org.agnitas.target.Target, org.agnitas.util.AgnUtils, java.util.*, java.text.DateFormat"  errorPage="/error.jsp" %>
<%@ page import="org.agnitas.util.EmmCalendar" %>
<%@ page import="org.agnitas.util.SafeString" %>
<%@ page import="org.agnitas.web.MailingStatAction" %>
<%@ page import="org.agnitas.web.MailingStatForm" %>
<%@ page import="java.util.HashSet" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.LinkedList" %>
<%@ page import="java.util.ListIterator" %>
<%@ page import="java.util.Locale" %>
<%@ page import="java.util.TimeZone" %>
<%@ taglib uri="/WEB-INF/agnitas-taglib.tld" prefix="agn" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<% int tmpMailingID = 0;
    int tmpTargetID = 0;
    // int tmpUniqueClicks=0;
    String tmpNetto = "no";
    String tmpShortname = new String("");
    MailingStatForm aForm = null;
    int maxblue = 0;
    int maxNRblue = 0;
    int maxSubscribers = 0;
    if (session.getAttribute("mailingStatForm") != null) {
        aForm = (MailingStatForm) session.getAttribute("mailingStatForm");
        tmpMailingID = aForm.getMailingID();
        tmpTargetID = aForm.getTargetID();
        tmpShortname = aForm.getMailingShortname();
        maxblue = aForm.getMaxblue();
        maxNRblue = aForm.getMaxNRblue();
        maxSubscribers = aForm.getMaxSubscribers();
    }

    java.text.DecimalFormat prcFormat = new java.text.DecimalFormat("##0.#");
    prcFormat.setDecimalSeparatorAlwaysShown(false);

    EmmCalendar my_calendar = new EmmCalendar(java.util.TimeZone.getDefault());
    my_calendar.changeTimeWithZone(TimeZone.getTimeZone(AgnUtils.getAdmin(request).getAdminTimezone()));
    java.util.Date my_time = my_calendar.getTime();
    String Datum = my_time.toString();
    String timekey = Long.toString(my_time.getTime());
    pageContext.setAttribute("time_key", timekey);

    // map for the csv download
    java.util.Hashtable my_map = null;
    if (pageContext.getSession().getAttribute("map") == null) {
        my_map = new java.util.Hashtable();
        pageContext.getSession().setAttribute("map", my_map);
        // System.out.println("map exists.");
    } else {
        my_map = (java.util.Hashtable) (pageContext.getSession().getAttribute("map"));
        // System.out.println("new map.");
    }
    // put csv file from the form in the hash table:
    String file = "";

    DateFormat dateFormat=DateFormat.getDateInstance(DateFormat.MEDIUM, (Locale)session.getAttribute(org.apache.struts.Globals.LOCALE_KEY));
    DeliveryStat deliveryStat = (DeliveryStat)request.getAttribute("deliveryStat");

    Mailing mailing = (Mailing)request.getAttribute("mailing");
%>

<html:form action="/mailing_stat">
    <html:hidden property="mailingID"/>
    <html:hidden property="action"/>

    <div class="button_container">

        <div class="action_button float_left">
            <html:link page='<%= \"/ecs_stat.do?mailingId=\" + tmpMailingID + \"&init=true\" %>'>
                <span><bean:message key="ecs.Heatmap"/></span>
            </html:link>
        </div>

    </div>

    <% //prepare loop over targetIDs:
        Hashtable statValues = new Hashtable();
        statValues = ((MailingStatForm) session.getAttribute("mailingStatForm")).getStatValues();
        List<Integer> targets = null;
        ListIterator targetIter = null;
        targets = ((MailingStatForm) session.getAttribute("mailingStatForm")).getTargetIDs();
    %>

    <div class="content_element_container">
        <div class="target_group_select_panel">
            <div class="compare_view_group_container targetgroups_select_container">
                <% /* * * * * * * * * * */ %>
                <% /* add target group  */ %>
                <% /* * * * * * * * * * */ %>
                <% if (targets.size() < 5) { %>
                <bean:message key="target.Target"/>:
                <html:select property="nextTargetID">
                    <html:option value="0"><bean:message key="statistic.All_Subscribers"/></html:option>
                    <c:forEach var="target" items="${targetList}">
                        <% if (!targets.contains(new Integer(((Target) pageContext.getAttribute("target")).getId()))) { %>
                        <html:option value='${target.id}'>
                            ${target.targetName}
                        </html:option>
                        <% } %>
                    </c:forEach>
                </html:select>
                <% } %>
            </div>
            <% if (targets.size() < 5) { %>
            <div class="action_button add_button">
                <input type="hidden" name="add" value="">
                <a href="#"
                   onclick="document.mailingStatForm.add.value='add';document.mailingStatForm.submit();return false;">
                    <span><bean:message key="button.Add"/></span>
                </a>
            </div>
            <% } %>
        </div>
        <div align="right" >
            <html:link style="align:right" page='<%= new String(\"/file_download?key=\" + timekey) %>'><img
                    src="${emmLayoutBase.imagesURL}/icon_save.gif" border="0">
            </html:link>
        </div>
    </div>

    <div class="content_element_container">

        <table border="0" cellspacing="0" cellpadding="10" style="width: 100%;">

            <% /* * * * * * * * * * */ %>
            <% /* STATISTICHE INVIO */ %>
            <% /* * * * * * * * * * */ %>
            <tr>
                <td colspan="4"><span class="head3"><bean:message key="statistic.SendStats"/>:<br><br></span></td>
            </tr>

            <%
                file += "\"" + SafeString.getLocaleString("statistic.SendStats", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) + "\"";
            %>

            <% /* * * * * * * * * */ %>
            <% /* DETTAGLI INVIO  */ %>
            <% /* * * * * * * * * */ %>
            <tr>
                <td class="stats_normal_color" colspan="4"><b><bean:message key="statistic.SendDetails"/></b></td>
            </tr>

            <%
                file += "\r\n\r\n\"" + SafeString.getLocaleString("statistic.SendDetails", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) + "\"";
                file += "\r\n\r\n\"" + SafeString.getLocaleString("statistic.Newsletter", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) + "\"";
                file += ";\""+mailing.getShortname()+"\"";
            %>

            <tr>
                <td><b><bean:message key="statistic.Newsletter"/></b></td>
                <td colspan="3">${mailing.shortname}</td>
            </tr>
            <%
                int sentTotal = deliveryStat.getTotalMails();
                int openedTotal = 0;
                int deliveredTotal = deliveryStat.getSentMails();
                int clickTotal = 0;
                int clickedTotal = 0;
                int unsubscriptions = aForm.getOptOuts();
                targetIter = targets.listIterator();
                while (targetIter.hasNext()) {
                    int aktTargetID = ((Integer) targetIter.next()).intValue();
                    MailingStatEntry aktMailingStatEntry = (MailingStatEntry) statValues.get(new Integer(aktTargetID));
                    openedTotal += aktMailingStatEntry.getOpened();
                    clickTotal += aktMailingStatEntry.getTotalClicks();
                    clickedTotal += aktMailingStatEntry.getTotalClicksNetto();
                }
                int linkTotal = ((MailingStatForm) session.getAttribute("mailingStatForm")).getUrlNames().size();

                file += "\r\n\"" + SafeString.getLocaleString("statistic.SentOn", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) + "\"";
                file += ";\""+dateFormat.format(deliveryStat.getLastDate())+"\"";
                file += "\r\n\"" + SafeString.getLocaleString("statistic.SentTo", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) + "\"";
                file += ";\""+sentTotal+" "+SafeString.getLocaleString("statistic.emailAddresses", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) + "\"";
                file += "\r\n\"" + SafeString.getLocaleString("statistic.SubjectName", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) + "\"";
                file += ";\""+mailing.getDescription()+"\"";

            %>
            <tr>
                <td><b><bean:message key="statistic.SentOn"/></b></td>
                <td><%=dateFormat.format(deliveryStat.getLastDate())%></td>
                <td><b><bean:message key="statistic.SentTo"/></b></td>
                <td><%=sentTotal%> <bean:message key="statistic.emailAddresses"/></td>
            </tr>
            <tr>
                <td><b><bean:message key="statistic.SubjectName"/></b></td>
                <td>${mailing.description}</td>
            </tr>

            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>

            <% /* * * * * * * * * * * */ %>
            <% /* MESSAGGI RECAPITATI */ %>
            <% /* * * * * * * * * * * */ %>
            <tr>
                <td class="stats_normal_color" colspan="4"><b><bean:message key="statistic.MessagesSuccessfullyDelivered"/></b></td>
            </tr>

            <tr>
                <td><b><bean:message key="statistic.DeliveredTo"/></b></td>
                <td><%=deliveredTotal%> / <%=sentTotal%> <bean:message key="statistic.emailAddresses"/></td>
                <td><%=sentTotal - deliveredTotal%> <bean:message key="statistic.undelivered"/></td>
            </tr>

            <%
                file += "\r\n\r\n\r\n\"" + SafeString.getLocaleString("statistic.MessagesSuccessfullyDelivered", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) + "\"";
                file += "\r\n\r\n\"" + SafeString.getLocaleString("statistic.DeliveredTo", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) + "\"";
                file += ";\""+deliveredTotal+" / "+sentTotal+" "+SafeString.getLocaleString("statistic.emailAddresses", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) + "\"";
                file += ";\""+(sentTotal - deliveredTotal)+" "+SafeString.getLocaleString("statistic.undelivered", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY))+"\"";

            %>

            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>

            <% /* * * * * * * * */ %>
            <% /* MONITORAGGIO  */ %>
            <% /* * * * * * * * */ %>
            <tr>
                <td class="stats_normal_color" colspan="4"><b><bean:message key="statistic.MonitoringResult"/></b></td>
            </tr>

            <%
                file += "\r\n\r\n\r\n\"" + SafeString.getLocaleString("statistic.MonitoringResult", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) + "\"";
            %>
            <tr>
                <td><b><bean:message key="statistic.OpeningRate"/></b></td>
                <td>
                    <span style="display: inline-block; width: 80px;"><b><bean:message key="statistic.gross"/></b></span>
                    <% if(openedTotal > 0) { %>
                    <%=prcFormat.format((double)openedTotal / (double)sentTotal * 100d)%>%
                    <% } else { %>
                    0%
                    <% } %>
                </td>
                <td colspan="2">
                    <%=openedTotal%> / <%=sentTotal%> <bean:message key="statistic.emailAddresses"/>
                </td>
            </tr>
            <%
                file += "\r\n\r\n\"" + SafeString.getLocaleString("statistic.OpeningRate", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) + "\"";
                file += ";\""+SafeString.getLocaleString("statistic.gross", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) + "\"";
                file += ";\""+ (openedTotal > 0 ? prcFormat.format((double)openedTotal / (double)sentTotal * 100d) : 0) + "%\"";
                file += ";\""+ openedTotal + " / " + sentTotal +  SafeString.getLocaleString("statistic.emailAddresses", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) +"\"";
            %>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <span style="display: inline-block; width: 80px;"><b><bean:message key="statistic.net"/></b></span>
                    <% if(openedTotal > 0) { %>
                    <%=prcFormat.format((double)openedTotal / (double)deliveredTotal * 100d)%>%
                    <% } else { %>
                    0%
                    <% } %>
                </td>
                <td colspan="2">
                    <%=openedTotal%> / <%=deliveredTotal%> <bean:message key="statistic.netEmailAddresses"/>
                </td>
            </tr>
            <%
                file += "\r\n\"\"";
                file += ";\""+SafeString.getLocaleString("statistic.net", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) + "\"";
                file += ";\""+ (openedTotal > 0 ? prcFormat.format((double)openedTotal / (double)deliveredTotal * 100d) : 0) + "%\"";
                file += ";\""+ openedTotal + " / " + deliveredTotal +  SafeString.getLocaleString("statistic.emailAddresses", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) +"\"";
            %>
            <tr>
                <td colspan="4"><hr/></td>
            </tr>

            <tr>
                <td><b><bean:message key="statistic.ClickRate"/></b></td>
                <td>
                    <span style="display: inline-block; width: 80px;"><b><bean:message key="statistic.gross"/></b></span>
                    <% if(clickedTotal > 0) { %>
                    <%=prcFormat.format((double)clickedTotal / (double)sentTotal * 100d)%>%
                    <% } else { %>
                    0%
                    <% } %>
                </td>
                <td colspan="2">
                    <%=clickedTotal%> / <%=sentTotal%> <bean:message key="statistic.emailAddresses"/>
                </td>
            </tr>
            <%
                file += "\r\n\r\n\"" + SafeString.getLocaleString("statistic.ClickRate", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) + "\"";
                file += ";\""+SafeString.getLocaleString("statistic.gross", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) + "\"";
                file += ";\""+ (clickedTotal > 0 ? prcFormat.format((double)clickedTotal / (double)sentTotal * 100d) : 0) + "%\"";
                file += ";\""+ clickedTotal + " / " + sentTotal +  SafeString.getLocaleString("statistic.emailAddresses", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) +"\"";
            %>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <span style="display: inline-block; width: 80px;"><b><bean:message key="statistic.net"/></b></span>
                    <% if(clickedTotal > 0) { %>
                    <%=prcFormat.format((double)clickedTotal / (double)deliveredTotal * 100d)%>%
                    <% } else { %>
                    0%
                    <% } %>
                </td>
                <td colspan="2">
                    <%=clickedTotal%> / <%=deliveredTotal%> <bean:message key="statistic.netEmailAddresses"/>
                </td>
            </tr>
            <%
                file += "\r\n\"\"";
                file += ";\""+SafeString.getLocaleString("statistic.net", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) + "\"";
                file += ";\""+ (clickedTotal > 0 ? prcFormat.format((double)clickedTotal / (double)deliveredTotal * 100d) : 0) + "%\"";
                file += ";\""+ clickedTotal + " / " + deliveredTotal +  SafeString.getLocaleString("statistic.emailAddresses", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) +"\"";
            %>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <span style="display: inline-block; width: 80px;"><b><bean:message key="statistic.clean"/></b></span>
                    <% if(clickedTotal > 0) { %>
                    <%=prcFormat.format((double)clickedTotal / (double)openedTotal * 100d)%>%
                    <% } else { %>
                    0%
                    <% } %>
                </td>
                <td colspan="2">
                    <%=clickedTotal%> / <%=openedTotal%> <bean:message key="statistic.readers"/>
                </td>
            </tr>
            <%
                file += "\r\n\"\"";
                file += ";\""+SafeString.getLocaleString("statistic.clean", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) + "\"";
                file += ";\""+ (clickedTotal > 0 ? prcFormat.format((double)clickedTotal / (double)openedTotal * 100d) : 0) + "%\"";
                file += ";\""+ clickedTotal + " / " + openedTotal +  SafeString.getLocaleString("statistic.readers", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) +"\"";
            %>

            <tr>
                <td colspan="4"><hr/></td>
            </tr>

            <tr>
                <td><b><bean:message key="statistic.TotalAccesses"/></b></td>
                <td><%=clickTotal%></td>
                <td><bean:message key="statistic.TotalAccessesOn"/></td>
                <td><%=linkTotal%> <bean:message key="statistic.links"/></td>
            </tr>
            <%
                file += "\r\n\r\n\"" + SafeString.getLocaleString("statistic.TotalAccesses", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) + "\"";
                file += ";\""+clickTotal + "\"";
                file += "\r\n\"" + SafeString.getLocaleString("statistic.TotalAccessesOn", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) + "\"";
                file += ";\""+linkTotal + SafeString.getLocaleString("statistic.links", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) + "\"";
            %>

            <tr>
                <td colspan="4"><hr/></td>
            </tr>

            <tr>
                <td><b><bean:message key="statistic.Unsubscriptions"/></b></td>
                <td>
                    <span style="display: inline-block; width: 80px;"><b><bean:message key="statistic.net"/></b></span>
                    <% if(unsubscriptions > 0) { %>
                    <%=prcFormat.format((double)unsubscriptions / (double)deliveredTotal * 100d)%>%
                    <% } else { %>
                    0%
                    <% } %>
                </td>
                <td><%=unsubscriptions%> / <%=deliveredTotal%> <bean:message key="statistic.emailAddresses"/></td>
            </tr>

            <%
                file += "\r\n\r\n\"" + SafeString.getLocaleString("statistic.Unsubscriptions", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) + "\"";
                file += ";\""+SafeString.getLocaleString("statistic.net", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) + "\"";
                file += ";\""+ (unsubscriptions > 0 ? prcFormat.format((double)unsubscriptions / (double)deliveredTotal * 100d) : 0) + "%\"";
                file += ";\""+ unsubscriptions + " / " + deliveredTotal +  SafeString.getLocaleString("statistic.emailAddresses", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) +"\"";
            %>

            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>

        </table>

        <div id="openingChart" style="width: 100%; height: 300px; margin: 10px 0;"></div>
        <div id="linkMonitoringChart" style="width: 100%; height: 300px; margin: 10px 0;"></div>

        <table border="0" cellspacing="0" cellpadding="10" style="width: 100%;">

            <% /* * * * * * * * * * */ %>
            <% /* MONITORAGGIO LINK */ %>
            <% /* * * * * * * * * * */ %>
            <tr>
                <td colspan="4"><span class="head3"><bean:message key="statistic.LinksMonitoring"/>:<br><br></span></td>
            </tr>

            <%
                file += "\r\n\r\n\r\n\"" + SafeString.getLocaleString("statistic.LinksMonitoring", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) + "\"";
                file += "\r\n\r\n\"" + SafeString.getLocaleString("statistic.LinkWebAddress", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) + "\"";
                file += ";\"" + SafeString.getLocaleString("statistic.Accesses", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) + "\"";
                file += ";\"" + SafeString.getLocaleString("statistic.PercAccessesOnDelivered", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY)) + "\"";
            %>

            <tr class="stats_normal_color">
                <td colspan="2"><b><bean:message key="statistic.LinkWebAddress"/></b></td>
                <td><b><bean:message key="statistic.Accesses"/></b></td>
                <td><b><bean:message key="statistic.PercAccessesOnDelivered"/></b></td>
            </tr>

            <tr>
                <td colspan="4"><hr/></td>
            </tr>

            <% boolean changeColor = true; %>

            <% /* * * * * * * * * * * */ %>
            <% /* loop over all URLs  */ %>
            <% /* * * * * * * * * * * */ %>
            <%
                int aktTargetID = 0;
                int aktUrlID = 0;
                Hashtable urlNames = new Hashtable();
                Hashtable urlShortnames = new Hashtable();
                urlNames = ((MailingStatForm) session.getAttribute("mailingStatForm")).getUrlNames();
                urlShortnames = ((MailingStatForm) session.getAttribute("mailingStatForm")).getUrlShortnames();
                java.text.NumberFormat nf = java.text.NumberFormat.getCurrencyInstance((Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY));
                LinkedList tmpClickedUrls = new LinkedList();
                tmpClickedUrls = aForm.getClickedUrls();
                HashSet map = new HashSet();
                int urlIndex = tmpClickedUrls.size();
                while (--urlIndex >= 0) {
                    aktUrlID = ((URLStatEntry) (tmpClickedUrls.get(urlIndex))).getUrlID();
                    map.add(new Integer(aktUrlID));
                }
                urlIndex = map.size();
                while (--urlIndex >= 0) {
                    aktUrlID = ((URLStatEntry) (tmpClickedUrls.get(urlIndex))).getUrlID();
                    TrackableLink trkLnk = (TrackableLink) urlNames.get(new Integer(aktUrlID));

            %>
            <% if (changeColor) { %>
            <tr class="stats_normal_color">
                        <% } else { %>
            <tr>
                <% }
                    changeColor = !changeColor; %>
                <td colspan="2" valign="center">
                    <a href='<%= trkLnk != null ? trkLnk.getFullUrl():"" %>' target="_blank">
                        <img src="${emmLayoutBase.imagesURL}/extlink.gif" border="0"
                             alt="<%= urlNames.get(new Integer(aktUrlID)) %>">
                    </a>&nbsp;
                    <html:link styleClass="blue_link"
                               page='<%= new String(\"/mailing_stat.do?action=\" + MailingStatAction.ACTION_WEEKSTAT + \"&mailingID=\" + tmpMailingID + \"&urlID=\" + aktUrlID + \"&targetID=0\") %>'>
                        <% if (((String) urlShortnames.get(new Integer(aktUrlID))).compareTo("") != 0) { %><%= urlShortnames.get(new Integer(aktUrlID)) %><% } else { %><%= urlNames.get(new Integer(aktUrlID)) %><% } %>
                    </html:link>&nbsp;
                </td>
                <%
                    int totalClicks = 0;
                    targetIter = targets.listIterator();
                    while (targetIter.hasNext()) {
                        aktTargetID = ((Integer) targetIter.next()).intValue();
                        MailingStatEntry aktMailingStatEntry = (MailingStatEntry) statValues.get(new Integer(aktTargetID));
                        Hashtable aktClickStatValues = (Hashtable) aktMailingStatEntry.getClickStatValues();
                        URLStatEntry aktURLStatEntry = (URLStatEntry) aktClickStatValues.get(new Integer(aktUrlID));
                        totalClicks += aktURLStatEntry.getClicks();
                    }
                %>
                <td>
                    <%=totalClicks%>
                </td>
                <td>
                    <%=totalClicks > 0 ? prcFormat.format((double)totalClicks / (double) deliveredTotal * 100d) : 0 %>%
                </td>
            </tr>
            <%
                    file += "\r\n\"" + (((String) urlShortnames.get(new Integer(aktUrlID))).compareTo("") != 0 ? urlShortnames.get(new Integer(aktUrlID)) : urlNames.get(new Integer(aktUrlID))) + "\"";
                    file += ";\"" + totalClicks + "\"";
                    file += ";\"" + (totalClicks > 0 ? prcFormat.format((double)totalClicks / (double) deliveredTotal * 100d) : 0) + "\"";
                }

                // * * * * * * * * * * * * *
                // * *  outer loop end:  * *
                // * * * * * * * * * * * * *
            %>


            <% /* * * * * * * * * * * */ %>
            <% /* clean admin clicks  */ %>
            <agn:ShowByPermission token="stats.clean">
                <tr>
                    <td colspan="<%=(targets.size() + 1)%>" align="right"><br><html:link styleClass="blue_link"
                                                                                         page='<%= new String(\"/mailing_stat.do?action=\" + MailingStatAction.ACTION_CLEAN_QUESTION + \"&mailingID=\" + tmpMailingID) %>'><bean:message
                            key="statistic.DeleteAdminClicks"/></html:link></td>
                </tr>
            </agn:ShowByPermission>

        </table>

        <br/><br/>

        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <script type="text/javascript">
            google.load('visualization', '1.0', {'packages':['corechart']});

            google.setOnLoadCallback(drawCharts);

            function drawCharts() {
                var data = new google.visualization.DataTable();
                data.addColumn('string', '<%=SafeString.getLocaleString("statistic.OpeningRate", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY))%>');
                data.addColumn('number', '<%=SafeString.getLocaleString("statistic.Openings", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY))%>');
                data.addRows([
                    ['<%=SafeString.getLocaleString("statistic.NotOpened", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY))%>', <%=sentTotal-openedTotal%>],
                    ['<%=SafeString.getLocaleString("statistic.OpenedButNotClicked", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY))%>', <%=openedTotal-clickedTotal%>],
                    ['<%=SafeString.getLocaleString("statistic.OpenedAndClicked", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY))%>', <%=clickedTotal%>]
                ]);

                var options = {
                    'title':'<%=SafeString.getLocaleString("statistic.OpeningRate", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY))%>',
                    'height':300,
                    'hAxis': { format: '#%' },
                };
                var chart = new google.visualization.PieChart(document.getElementById('openingChart'));
                chart.draw(data, options);


                data = new google.visualization.DataTable();
                data.addColumn('string', '<%=SafeString.getLocaleString("statistic.ClickRate", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY))%>');
                data.addColumn('number', '<%=SafeString.getLocaleString("statistic.Clicks", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY))%>');
                data.addRows([
                    ['<%=SafeString.getLocaleString("statistic.gross", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY))%>', <%=clickedTotal > 0 ? prcFormat.format((double)clickedTotal / (double)sentTotal * 100d) : 0%>],
                    ['<%=SafeString.getLocaleString("statistic.net", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY))%>', <%=clickedTotal > 0 ? prcFormat.format((double)clickedTotal / (double)deliveredTotal * 100d) : 0%>],
                    ['<%=SafeString.getLocaleString("statistic.clean", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY))%>', <%=clickedTotal > 0 ? prcFormat.format((double)clickedTotal / (double)openedTotal * 100d) : 0%>],
                ]);

                options = {
                    'title':'<%=SafeString.getLocaleString("statistic.ClickRate", (Locale) request.getSession().getAttribute(org.apache.struts.Globals.LOCALE_KEY))%>',
                    'height':300,
                    'hAxis': { format: '#\'%\'' },
                    'legend': {position: 'none'}
                };

                chart = new google.visualization.BarChart(document.getElementById('linkMonitoringChart'));
                chart.draw(data, options);

            }

        </script>

    </div>

    <%
        my_map.put(timekey, file);
    /*
   System.out.println("#######################################");
   System.out.println(file);
   System.out.println("#######################################");
    */
        pageContext.getSession().setAttribute("map", my_map);

    %>

</html:form>